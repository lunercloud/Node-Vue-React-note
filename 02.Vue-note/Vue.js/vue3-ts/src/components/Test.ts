import { defineComponent, reactive, onMounted } from 'vue'

interface Book {
  title: string
  year?: number
}

function useCreateBook() {
  const book = reactive<Book>({
    title: 'html5',
    year: 2021
  })

  onMounted(() => {
    book.title = 'css3'
  })

  return {
    book
  }
}

export default useCreateBook