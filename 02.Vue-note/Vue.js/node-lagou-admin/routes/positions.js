var express = require('express');
var router = express.Router();

const { list, add, listone, update, remove } = require('../controllers/positions')
const uploadMiddleWare  = require('../middlewares/upload')

router.get('/list', list)
router.post('/listone', listone)
router.post('/upload', uploadMiddleWare)
router.post('/update', update)
router.post('/remove', remove)
router.post('/add', add)

module.exports = router
