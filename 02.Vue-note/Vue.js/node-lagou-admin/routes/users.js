var express = require('express');
var router = express.Router();

const { signin, list, signup, remove } = require('../controllers/users')

router.post('/signin', signin)
router.post('/signup', signup)
router.delete('/', remove)
router.get('/list', list)

module.exports = router
