const Mock = require('mockjs')

const data = Mock.mock({
  'items|10': [{
    id: '@id'
  }]
})

module.exports = [
  {
    url: '/vue-admin-template/test',
    type: 'get',
    response: config => {
      const items = data.items
      return {
        code: 20000,
        data: {
          total: items.length,
          items: items
        }
      }
    }
  }
]
