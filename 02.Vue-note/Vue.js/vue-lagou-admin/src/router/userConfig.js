export default [{
  path: '/test',
  component: Message,
  children: [
    {
      path: 'index',
      name: 'Test',
      component: () => import('@/views/form/index'),
      meta: { title: 'Test', icon: 'form' }
    }
  ]
}]