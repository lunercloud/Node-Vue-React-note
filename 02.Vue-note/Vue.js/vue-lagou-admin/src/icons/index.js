import Vue from 'vue'
import SvgIcon from '@/components/SvgIcon'// svg component

// register globally
Vue.component('svg-icon', SvgIcon)

const r = require.context('./svg', false, /\.svg$/)
const requireAll = r => r.keys().map(r)
requireAll(r)
