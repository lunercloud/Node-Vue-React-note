import request from '@/utils/request'

export function list() {
  return request({
    url: '/positions/list'
  })
}

export function listone(data) {
  return request({
    url: '/positions/listone',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/positions/remove',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/positions/update',
    method: 'post',
    data
  })
}

export function add(data) {
  return request({
    url: '/positions/add',
    method: 'post',
    data
  })
}