import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/users/signin',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/users',
    method: 'delete',
    data
  })
}

export function signup(data) {
  return request({
    url: '/users/signup',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/vue-admin-template/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  // return request({
  //   url: '/vue-admin-template/user/logout',
  //   method: 'post'
  // })
  return Promise.resolve()
}

export function getList() {
  return request({
    url: '/users/list',
    method: 'get'
  })
}
