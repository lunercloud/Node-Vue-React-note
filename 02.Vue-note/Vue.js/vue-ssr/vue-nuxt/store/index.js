module.exports = {
  state: {
    count: 0
  },

  mutations: {
    add(state) {
      state.count++
    }
  }
}