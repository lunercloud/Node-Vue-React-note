var express = require('express');
var router = express.Router();
var ejs = require('ejs')
var fs = require('fs')
var path = require('path')
var Vue = require('vue')
var VSR = require('vue-server-renderer')

/* GET home page. */
router.get('/ssr', function(req, res, next) {
  // res.render('index', { title: 'Express' });
  // const str = fs.readFileSync(path.resolve(__dirname, '../views/index.ejs'))
  // let template = ejs.compile(str.toString())
  // let result = template({ title: 'Express' })
  // fs.writeFileSync(path.resolve(__dirname, '../public/index.html'), result)
  // res.send('发布成功')

  const app = new Vue({
    template: `<div>{{title}}</div>`,
    data: {
      title: 'hello'
    }
  })

  const renderer = VSR.createRenderer()

  renderer.renderToString(app, (err, html) => {
    res.send(html)
  })
});

module.exports = router;
