import Vue from 'vue'
import App from './App.vue'
import * as http from '@/utils/http'

import './assets/reset.css'
import './assets/common.css'
import router from './router'
import store from './store'

import '@/filters/movie'

import 'animate.css'

Vue.config.productionTip = false

// 保存http到Vue的原型上
Vue.prototype.$http = http

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})