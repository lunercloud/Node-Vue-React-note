import axios from 'axios'

const instance = axios.create({
  // baseURL: 'http://localhost:8080/',
  timeout: 600000,
  // headers: {'content-type': 'application/x-www-form-encoded'}
})

// 添加请求拦截器
instance.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  config.headers = {
    'X-Host': 'mall.film-ticket.city.list'
  }

  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
});

const get = ({url, params}) => {
  // 为了捕获错误，我们自己封装了一个Promise
  return new Promise((resolve, reject) => {
    // axios 请求
    instance.get(
      url,
      {
        params
      }
    )
    .then((result) => {
      resolve(result.data)
    })
    .catch((err) => {
      reject(err.message)
    })
  })
}

export {
  get
}