import Vue from 'vue'
Vue.filter('wh', (value, size) => {
  return value.replace('w.h', size)
})
Vue.filter('ver', (value) => {
  let str = ''
  if (!!value) {
    let reg = new RegExp('(3d)|(2d)|(imax) ', 'gi')
    let arr = value.match(reg)

    if (arr.includes('3D')) {
      str += ' v3d '
    }
    if (arr.includes('IMAX ')) {
      str += ' imax '
    }
  }
  return str
})