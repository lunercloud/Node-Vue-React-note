import Vue from 'vue'
import Vuex, { Store } from 'vuex'

Vue.use(Vuex)

const store = new Store({
  state: {
    cityId: 0,
    cityName: '北京'
  },

  mutations: {
    setCity(state, city) {
      state.cityId = city.cityId
      state.cityName = city.name
    }
  },

  actions: {
    changeCity({commit}, city) {
      commit('setCity', city)
    }
  }
})

export default store