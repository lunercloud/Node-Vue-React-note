import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/home/Index'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
    redirect: '/home/movies',
    children: [
      {
        path: 'movies',
        name: 'movies',
        component: () => import(/* webpackChunkName: "movies" */ '@/views/home/movies/Movie'),
        children: [
          {
            path: '/home/movies',
            redirect: '/home/movies/intheaters'
          },
          {
            path: 'intheaters',
            name: 'intheaters',
            meta: 1,
            component: () => import(/* webpackChunkName: "intheaters" */ '@/views/home/movies/Intheater'),
          },
          {
            path: 'comingsoon',
            name: 'comingsoon',
            meta: 1,
            component: () => import(/* webpackChunkName: "comingsoon" */ '@/views/home/movies/ComingSoon'),
          }
        ]
      },
      {
        path: 'theaters',
        name: 'theaters',
        component: () => import(/* webpackChunkName: "theaters" */ '@/views/home/theaters/Theaters')
      },
      {
        path: 'profile',
        name: 'profile',
        component: () => import(/* webpackChunkName: "profile" */ '@/views/home/profile/Profile')
      }
    ]
  },
  {
    path: '/citypicker',
    name: 'citypicker',
    component: () => import(/* webpackChunkName: "citypicker" */ '@/views/citypicker/CityPicker')
  },
  {
    path: '/details',
    name: 'details',
    meta: 2,
    component: () => import(/* webpackChunkName: "details" */ '@/views/details/Details')
  }
] 

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router