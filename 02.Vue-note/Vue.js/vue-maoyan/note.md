# 布局方案
- 100%布局（流式布局）
  - 宽度100%，高度固定，比较适合list布局
  - 控件弹性
  - 文字流式
  - 图片等比例
- rem布局（等比缩放布局）
  - 所有的手机屏幕看到的内容一样
  - 首页，展示页面，游戏页面
  - vw: 当前屏幕宽度的 1%
  - vh: 当前屏幕高度的 1%
  - iphone5为例：1vw = 3.2px
  - iphone6为例：1vw = 3.75px
- 响应式布局（媒体查询）
  - 移动端和PC端用一套样式

# 布局技术
- css3 盒模型
- 定位
- flexbox
- mediaquires
- float

# 工具
- charles
- insomnia

# 路由
- SPA（single page application）
- MPA（multiple page application）


https://wx.maoyan.com/hostproxy/dianying/cities.json

https://m.maoyan.com/ajax/comingList?ci=1&token=&limit=10&optimus_uuid=C265B0800A1411EB9D14AFFCEB030AE4C16BA35ABD8C4380BB7BA4D8785C2D19&optimus_risk_level=71&optimus_code=10

https://wx.maoyan.com/mmdb/movie/v1/list/wish/order/comingList?ci=1&token=&limit=10&optimus_uuid=C265B0800A1411EB9D14AFFCEB030AE4C16BA35ABD8C4380BB7BA4D8785C2D19&optimus_risk_level=71&optimus_code=10
