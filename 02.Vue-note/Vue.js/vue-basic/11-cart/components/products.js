Vue.component('products', {
  template: `
    <ul>
      <li v-for="product in products">
        {{product.name}} : ￥{{product.price}} 元
        <button 
          @click="handleClick(product)"
          :disabled="product.inventory===0"
        >放入购物车</button>
      </li>
    </ul>
  `,

  data() {
    return {
      products: []
    }
  },

  methods: {
    handleClick(product) {
      this.$emit('receive',{...product})
      product.inventory--
    }
  },

  mounted() {
    fetch('/products.json')
      .then(response => response.json())
      .then(result => {
        this.products = result
      })
  },
})