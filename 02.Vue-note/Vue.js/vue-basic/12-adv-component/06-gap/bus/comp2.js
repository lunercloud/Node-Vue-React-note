import bus from './bus.js'
const comp2 = {
  template: '<div>comp2</div>',
  mounted() {
    bus.$emit('my-event', 'infomation')
    bus.$emit('add', 100)
    console.log(bus.x)
  },
}

export default comp2