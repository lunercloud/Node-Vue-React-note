import bus from './bus.js'

const comp1 = {
  template: '<div>comp1</div>',
  mounted() {
    bus.$on('my-event', (args) => {
      console.log(args)
    })
  },
}

export default comp1