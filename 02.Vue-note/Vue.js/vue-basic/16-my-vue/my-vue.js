debugger;
class Dep {
  constructor () {
    /* 用来存放 Watcher 对象的数组 */ 
    this.subs = [];
  }

  /* 在 subs 中添加一个 Watcher 对象 */ 
  addSub (sub) {
    this.subs.push(sub);
  }

  /* 通知所有 Watcher 对象更新视图 */ 
  notify () {
    this.subs.forEach((sub) => { 
      sub.update()
    })
  }
}

class Watcher {
  constructor () {
    /* 在 new 一个 Watcher 对象时将该对象赋值给 Dep.target，在 get 中会用到 */ 
    Dep.target = this
  }

  /* 更新视图的方法 */ 
  update() {
    console.log("视图更新啦~")
  }
}

Dep.target = null



function cb(val) {
  /* 渲染视图 */
  console.log("视图更新啦~")
}

function defineReactive(obj, key, val) {

  const dep = new Dep()

  Object.defineProperty(obj, key, {
    enumerable: true,
    /* 属性可枚举 */ 
    configurable: true,
    /* 属性可被修改或删除 */ 
    get: function reactiveGetter() {
      console.log('get')
      dep.addSub(Dep.target)
      return val; /* 实际上会依赖收集，下一小节会讲 */
    },
    set: function reactiveSetter(newVal) {
      if (newVal === val) return;
      // cb(newVal);
      dep.notify()
    }
  })
}

function observer (value) {
  if (!value || (typeof value !== 'object')) {
    return;
  }

  Object.keys(value).forEach((key) => { 
    defineReactive(value, key, value[key]);
  })
}

class Vue {
  constructor(opitons) {
    this._data = opitons.data
    observer(this._data)

    // new Watcher()

    // console.log(this._data.x)
  }
}

const vm = new Vue({
  data: {
    x: 0,
    y: 1
  }
})

// console.log(vm._data.x)
// vm._data.x = 100

// const value = {
//   x: 0,
//   y: 1
// }

// observer(value)
// defineReactive(value, 'x', 0);
// defineReactive(value, 'y', 1);
