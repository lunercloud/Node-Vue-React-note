const mixin = {
  data() {
    return {
      title: 'hello'
    }
  },
  methods: {
    handleClick() {
      console.log('in mixin')
    }
  },
  computed: {
    title2() {
      return this.title + '~~~'
    }
  },
  mounted() {
    console.log('mixin mouted.')
  },
}

export default mixin