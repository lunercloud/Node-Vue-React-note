const MyPlugin = {}

MyPlugin.install = (Vue, options) => {
  Vue.prototype.$gp22 = 'very good'
  Vue.prototype.$sayHello = () => {
    console.log('hello')
  }
  Vue.component('my-component', {
    template: `
      <p>hello</p>
    `
  })
  Vue.directive('font', (el, bindings) => {
    el.style.fontSize = bindings.value
  })
  Vue.filter('g', (value) => {
    return value + '人'
  })
  Vue.mixin({
    beforeRouter() {
      console.log(0)
    },
    mounted() {
      console.log(100)
    },
  })
}

export default MyPlugin