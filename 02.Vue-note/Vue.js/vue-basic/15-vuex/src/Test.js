class A {
  constructor () {
    this.name = 0
  }

  sayName() {
    return this.name
  }
}

const alert = () => {
  console.log(0)
}

A.alert = alert

export default A
export {
  alert
}