import axios from 'axios'

const product = {
  namespaced: true,

  state: {
    all: []
  },

  mutations: {
    addProduct(state, products) {
      state.all = products
    },

    setProduct(state, product) {
      const p = state.all.find(value => value.id === product.id)
      p.inventory--
    }
  },

  actions: {
    loadData({commit}) {
      axios({
        url: '/products.json'
      })
      .then((result) => {
        commit('addProduct', result.data)
      })
    },

    decrementInventory({commit}, product) {
      commit('setProduct', product)
    }
  }
}

export default product