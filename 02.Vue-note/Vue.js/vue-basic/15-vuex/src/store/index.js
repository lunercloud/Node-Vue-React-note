import Vue from 'vue'
import Vuex, { Store } from 'vuex'
Vue.use(Vuex)

import product from './modules/product'
import cart from './modules/cart'

export default new Store({
  modules: {
    product,
    cart
  }
})