import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import { ADDCOUNT } from './mutation-types'

Vue.use(Vuex)

const options = {
  state: {
    count: 0
  },

  mutations: {
    // 纯函数: 给定确定的输入，就会得到既定输出
    // 副作用函数：
    // return Math.random()
    // setCount(state, payload) {
    //   setTimeout(() => {
    //     // mutate: 人工
    //     state.count += payload
    //   }, 1000)
    // }
    // reduce: 归并
    [ADDCOUNT](state, payload) {
      // console.log(0)
      // mutate: 人工
      state.count += payload
    },
    minus(state) {
      state.count--
    }
  },

  actions: {
    add({ commit }, payload) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          // 此处省略一万行
          commit(ADDCOUNT, payload)
          resolve()
        }, 1000)
      })
    },
    minus() {
      setTimeout(() => {
        commit('minus')
      }, 1000)
    }
  },

  getters: {
    count3(state) {
      return 10000 + state.count
    },
    count4: (state) => {
      return (num) => {
        return state.count + num
      }
    }
  }
}

const store = new Store(options)

export default store