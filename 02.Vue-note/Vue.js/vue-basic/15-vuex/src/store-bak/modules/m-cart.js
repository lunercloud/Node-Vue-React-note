const carts = {
  namespaced: true,
  
  state: {
    all: ['c1']
  },

  mutations: {
    setCarts(state, data) {
      state.all = data
    }
  }
}

export default carts