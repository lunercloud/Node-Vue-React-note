import Vue from 'vue'
import Vuex, { Store } from 'vuex'
Vue.use(Vuex)

import product from './modules/m-products'
import cart from './modules/m-cart'

export default new Store({
  state: {
    count: 0
  },
  mutations: {
    add(state) {
      // state.count++
      console.log(1)
    }
  },
  modules: {
    product,
    cart
  }
})