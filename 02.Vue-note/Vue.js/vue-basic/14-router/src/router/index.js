import Vue from 'vue'
import VueRouter from 'vue-router'

import Movie from '../views/Movie.vue'
import Tv from '../views/Tv.vue'

import Comedy from '../views/movies/Comedy.vue'
import Children from '../views/movies/Children.vue'

import About from '../views/About.vue'
import Page404 from '../views/Page404.vue'
import User from '../views/User.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/movie'
  },
  {
    path: '/animation',
    component: () => import('@/views/Animation.vue')
  },
  {
    path: '/movie',
    component: Movie,
    redirect: '/movie/comedy',
    children: [
      {
        path: 'comedy',
        component: Comedy
      },
      {
        path: 'children',
        component: Children
      }
    ]
  },
  {
    path: '/tv',
    alias: '/a/b',
    components: {
      default: Tv,
      about: About
    },
    beforeEnter(to, from, next) {
      console.log(0)
      next()
    }
  },
  {
    path: '/env/:id',
    name: 'env',
    meta: {
      num: 100
    },
    component: () => import(/* webpackChunkName: "env" */ '../views/Env.vue')
  },
  {
    path: '/user-*',
    component: User
  },
  {
    path: '*',
    component: Page404
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router