## tools
- browser-sync
 - yarn global add browser-sync
 - browser-sync start -s -f **/* --watch --directory


## Vue 概念
- 指令（directive）
- reactive (响应式的)
- 生命周期函数（生命周期钩子）
- 插值表达式
- 数据双向绑定
- 单向数据流

## 架构概念
- SPA(single page application)
- MPA(multiple page application)

## 原理
- Object.defineProperty(x, {
  set() {
    // 观察者模式
  }
})
x = 1000

## 路由
- 包容性路由
- 排他性路由

## 状态
- 保存了某个数据的变量，称之为一个状态
- data: 组件内部独享的
- props: 父组件传给子组件的
- state: 全部组件共享的数据