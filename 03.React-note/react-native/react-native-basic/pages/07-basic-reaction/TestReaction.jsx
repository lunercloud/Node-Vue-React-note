import React from 'react'
import { View, Text, Button, TouchableHighlight } from 'react-native'

export default function TestReaction() {
  const handleClick = () => {
    console.log(100)
  }

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <TouchableHighlight 
        onPress={handleClick}
        underlayColor="white"
        activeOpacity={0.8}
      >
        {/* <Button 
          title="clike me" 
          onPress={handleClick}
          color="red"
        ></Button> */}
        <View style={{width: 150, height: 30, backgroundColor: 'tomato'}}>
          <Text style={{ textAlign: 'center', lineHeight: 30, color: 'white'}}>Click me</Text>
        </View>
      </TouchableHighlight>
    </View>
  )
}
