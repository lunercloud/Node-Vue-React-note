import React from 'react'
import { View, Text } from 'react-native'

import styles from './testView'

export default (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>hello</Text>
    </View>
  )
}