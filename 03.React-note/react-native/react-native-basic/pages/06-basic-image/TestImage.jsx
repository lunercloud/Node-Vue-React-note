import React from 'react'
import { View, Image, Text, ImageBackground } from 'react-native'

export default function TestImage() {
  return (
    <>
      <View>
        <Image
          source={require('../../assets/image_25.png')}
          style={{width: 100, height: 100}}
        ></Image>
        {/* <Image
          source={require('../../assets/a.mp4')}
        ></Image> */}
        {/* <Image
          source={{
            uri: 'https://facebook.github.io/react/logo-og.png'
          }}
          style={{width: 100, height: 100}}
        ></Image> */}
      </View>
      <View style={{position: 'relative'}}>
        <ImageBackground 
          source={{uri: 'https://facebook.github.io/react/logo-og.png'}} 
          style={{width: '100%', height: '100%'}}
        >
        </ImageBackground>
        <View style={{position: 'absolute', zIndex: 100}}>
          <Text style={{color: 'black'}}>Inside</Text>
        </View>
      </View>
    </>
  )
}
