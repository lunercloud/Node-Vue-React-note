import React, {useEffect} from 'react'
import { View, Text, StyleSheet, Platform } from 'react-native'

import BigButton from './BigButton'

export default function TestPlatform() {
  useEffect(() => {
    console.log(Platform.Version)
  }, [])

  return (
    <View style={styles.container}>
      <Text>hello</Text>
      <BigButton></BigButton>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 60,
    borderWidth: 1,
    borderColor: Platform.OS === 'ios' ? 'red' : 'green',
    ...Platform.select({
      ios: {
        backgroundColor: 'red',
        margin: 100,
      },
      android: {
        backgroundColor: 'blue',
        margin: 20
      }
    })
  }
})