import React from 'react'
import { View, Text, TextInput, StyleSheet } from 'react-native'

export default function TestTextInput() {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.textTnput}
        keyboardType="url"
        returnKeyType="google"
        placeholder="请输入内容"
      ></TextInput>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textTnput: {
    borderStyle: 'solid',
    borderColor: 'gray',
    borderWidth: 1,
    padding: 10,
    width: '100%'
  }
})