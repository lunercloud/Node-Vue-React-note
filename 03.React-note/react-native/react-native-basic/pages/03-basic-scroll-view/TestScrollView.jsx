import React from 'react';
import { View, ScrollView, Text } from 'react-native';


export default App = () => (
  <>
    <View><Text>hello world</Text></View>
    <View style={{height: 200}}>
      <ScrollView>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text> 
      </ScrollView>
    </View>
    <View style={{height: 200}}>
      <ScrollView
        horizontal={true}
      >
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text>
        <Text style={{ fontSize: 96 }}>Scroll me plz</Text> 
      </ScrollView>
    </View>
  </>
);