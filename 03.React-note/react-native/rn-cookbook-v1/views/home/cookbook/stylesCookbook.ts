import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1
  },

  header: {
    height: 64,
    backgroundColor: 'yellow',
  },

  list: {
    flex: 1
  },

  swiper: {
    height: 280
  }
})