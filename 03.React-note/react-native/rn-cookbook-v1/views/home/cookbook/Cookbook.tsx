import React from 'react'
import { View, Text, ScrollView } from 'react-native'

import styles from './stylesCookbook'
import CbSwiper from './CbSwiper'

export default function Cookbook() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text>header</Text>
      </View>
      <ScrollView style={styles.list}>
        <View style={styles.swiper}>
          <CbSwiper></CbSwiper>
        </View>
      </ScrollView>
    </View>
  )
}
