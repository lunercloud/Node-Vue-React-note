import React from 'react'
import { View, Text, Image, SafeAreaView } from 'react-native'

import Tabbar from '../../components/tabbar/Tabbar'
import TabbarItem from '../../components/tabbar/TabbarItem'

import styles from './homeStyle'

import Cookbook from '../home/cookbook/Cookbook'

export default function Home() {

  const [selectedTab, setSelectedTab] = React.useState<string>('cookbook')

  const handlePress = (tab: string) => {
    return () => {
      setSelectedTab(tab)
    }
  }

  return (
    <View style={{flex: 1}}>
      <Tabbar
        height={44}
        bgColor="#fff"
        textColor="#666"
        initTab="美食大全"
      >
        <TabbarItem
          icon={<Image style={styles.itemImg} source={require('../../assets/images/cookbook.png')} />}
          selectedIcon={<Image style={styles.itemImg} source={require('../../assets/images/cookbook-active.png')} />}
          isSelected={selectedTab === 'cookbook'}
          onPress={handlePress('cookbook')}
          title="美食大全"
        >
          <Cookbook></Cookbook>
        </TabbarItem>
        <TabbarItem
          icon={<Image style={styles.itemImg2} source={require('../../assets/images/menu.png')} />}
          selectedIcon={<Image style={styles.itemImg2} source={require('../../assets/images/menu-active.png')} />}
          isSelected={selectedTab === 'category'}
          onPress={handlePress('category')}
          title="分类"
        >
          <View><Text>category</Text></View>
        </TabbarItem>
        <TabbarItem
          icon={<Image style={styles.itemImg2} source={require('../../assets/images/location.png')} />}
          selectedIcon={<Image style={styles.itemImg2} source={require('../../assets/images/location-active.png')} />}
          isSelected={selectedTab === 'map'}
          onPress={handlePress('map')}
          title="地图"
        >
          <View><Text>map</Text></View>
        </TabbarItem>
        <TabbarItem
          icon={<Image style={styles.itemImg2} source={require('../../assets/images/more.png')} />}
          selectedIcon={<Image style={styles.itemImg2} source={require('../../assets/images/more-active.png')} />}
          isSelected={selectedTab === 'more'}
          onPress={handlePress('more')}
          title="更多"
        >
          <View><Text>more</Text></View>
        </TabbarItem>
      </Tabbar>
    </View>
  )
}
