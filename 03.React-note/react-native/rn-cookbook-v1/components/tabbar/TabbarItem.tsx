import React, { FunctionComponent, useEffect } from 'react'
import { View, Text, TouchableWithoutFeedback, GestureResponderEvent} from 'react-native'
import { getItemStyle } from './tabbarStyle'

interface Props {
  children: React.ReactNode,
  icon: React.ReactNode,
  selectedIcon: React.ReactNode,
  isSelected: boolean,
  onPress: () => void,
  title: string,
  textColor?: string,
  changeTab?: (ele: React.ReactNode) => void,
  initTab?: string
}

const TabbarItem:FunctionComponent<Props> = (props) => {
  let { itemWrapper, itemText, itemTextColor } = getItemStyle({textColor: props.textColor})
  let { isSelected, icon, selectedIcon, title } = props

  const _renderTab = () => {
    props.changeTab && props.changeTab(props.children)
  }

  useEffect(() => {
    if (props.initTab === props.title) {
      _renderTab()
    }
  }, [])
  
  const handlePress = () => {
    _renderTab()
    props.onPress()
  }

  return (
    <TouchableWithoutFeedback
      onPress={handlePress}
    >
      <View style={itemWrapper}>
        {isSelected ? selectedIcon : icon}
        <Text style={[itemText, !isSelected && itemTextColor]}>{title}</Text>
      </View>
    </TouchableWithoutFeedback>
  )
}

export default TabbarItem
