import React, { FunctionComponent, useState } from 'react'
import { View, SafeAreaView } from 'react-native'

import { getTabbarStyle} from './tabbarStyle'

interface Props {
  height: number,
  bgColor: string,
  children: React.ReactChild[],
  textColor: string,
  initTab: string
}

const Tabbar:FunctionComponent<Props> = (props) => {
  let { height, bgColor } = props
  let [ele, setEle] = useState<React.ReactNode | null>(null)

  let { tabbarBodyWrap, tabbarWrap, tabbarNavWrap } = getTabbarStyle({height, bgColor})

  const changeTab = (ele: React.ReactNode) => {
    setEle(ele)
  }

  return (
    <View style={tabbarWrap}>
      <View style={tabbarBodyWrap}>
        {ele}
      </View>
      <SafeAreaView>
        <View
          style={tabbarNavWrap}
        >
          {
            React.Children.map(props.children, (element: any) => {
              return React.cloneElement(element, {
                textColor: props.textColor,
                changeTab,
                initTab: props.initTab
              })
            })
          }
        </View>
      </SafeAreaView>
    </View>
  )
}

export default Tabbar
