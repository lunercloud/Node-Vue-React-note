import { StyleSheet } from 'react-native'

const getTabbarStyle = ({height, bgColor}) => {
  return StyleSheet.create({
    tabbarWrap: {
      flex: 1
    },

    tabbarBodyWrap: {
      flex: 1
    },

    tabbarNavWrap: {
      height,
      backgroundColor: bgColor,
      flexDirection: 'row',
      borderTopColor: '#eee',
      borderTopWidth: 1
    }
  })
}

const getItemStyle = ({textColor}) => {
  return StyleSheet.create({
    itemWrapper: {
      flexDirection: 'column',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },

    itemText: {
      fontSize: 10,
      marginTop: 2
    },

    itemTextColor: {
      color: textColor
    }
  })
}

export {
  getTabbarStyle,
  getItemStyle
}
