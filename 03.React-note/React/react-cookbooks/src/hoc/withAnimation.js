import React, { Component } from 'react'
import { CSSTransition } from 'react-transition-group'
import { withRouter } from 'react-router-dom'

const withAnimation = (Comp) => {
  @withRouter
  class Temp extends Component {
    render() {
      const match = this.props.match
      const pathname = this.props.location.pathname

      const inStyle = pathname === '/' ? 'slideInLeft' : 'slideInRight'
      const outStyle = pathname === '/' ? 'slideOutRight' : 'slideOutLeft'

      return (
        <CSSTransition
          in={!!match}
          timeout={1000}
          classNames={{
            enter: 'animate__animated',
            enterActive: 'animate__' + inStyle,
            exit: 'animate__animated',
            exitActive: 'animate__' + outStyle
          }}
          unmountOnExit
          mountOnEnter
        >
          <Comp { ...this.props }></Comp>
        </CSSTransition>
      )
    }
  }

  return Temp
}

export default withAnimation