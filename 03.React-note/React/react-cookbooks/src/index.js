import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import { BrowserRouter as Router } from 'react-router-dom'

import './weixin/config'

import App from './App'

import './assets/reset.css'
import 'animate.css'

// import Swiper styles
import 'swiper/css/swiper.min.css'

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App></App>
    </Router>
  </Provider>,
  document.querySelector('#root')
)

