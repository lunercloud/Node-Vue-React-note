import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import Home from '@/views/home/Home'
import List from '@/views/list/List'
import Detail from '@/views/detail/Detail'

class App extends Component {
  render() {
    return (
      <>
        <Route 
          path="/" 
          exact 
          children={() => <Home></Home>}
        ></Route>
        <Route
          path="/list"
          children={() => <List></List>}
        ></Route>

        <Route 
          path="/detail"
          children={() => <Detail></Detail>}
        ></Route>
      </>
    );
  }
}

export default App