import React from 'react'
import { useSelector } from 'react-redux'
import { useActions } from '@/hooks/useActions'
import { actions } from '@/views/home/category'

import {
  CategoryWrapper
} from './StyledCategory'


function Category(props) {

  const tab = useSelector(state => state.getIn(['cate', 'tab']))
  const actionsObj = useActions(actions)

  const handleTabClick = (key) => { 
    return () => {
      actionsObj.change(key)
    }
  }

  let { categoryData: data } = props

  console.log(tab)
  console.log(data)

  return (
    <CategoryWrapper
      borderWidth="1px 0 0 0"
    >
      <div>
        <ul className="menu-tab">
          {
            Object.keys(data).map(key => (
              <li 
                key={key}
                onClick={handleTabClick(key)}
                className={key===tab ? 'active': ''}
              ><span>{key}</span></li>
            ))
          }
        </ul>
      </div>
      <div>
        <ul className="menu-content">
          {
            data[tab].map((value, index) => (
              <li key={value + index}>{value}</li>
            ))
          }
        </ul>
      </div>
    </CategoryWrapper>
  )
}

export default Category;