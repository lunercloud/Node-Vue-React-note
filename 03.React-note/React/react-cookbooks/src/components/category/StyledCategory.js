import styled from 'styled-components'
import border from '@/assets/styled/border'

const CategoryWrapper = border(
  styled.div `
    flex: 1;
    display: flex;
    overflow: hidden;
    > div:first-child {
      width: .9rem;
      overflow-y: scroll;
      .menu-tab {
        display: flex;
        flex-direction: column;
        li {
          height: .5rem;
          line-height: .5rem;
          text-align: center;
          &.active {
            background-color: #fff;
            span {
              border-bottom: solid 0.01rem #ee742f;
            }
          }
        }
      }
    }

    > div:last-child {
      overflow-y: scroll;
      flex: 1;
      background-color: #fff;
      .menu-content {
        display: flex;
        flex-wrap: wrap;
        align-content: flex-start;
        li {
          width: 33.333333%;
          height: .5rem;
          line-height: .5rem;
          text-align: center;
        }
      }
    }

    span {
      display: inline-block;
      height: 100%;
    }
  `
)

export {
  CategoryWrapper
}