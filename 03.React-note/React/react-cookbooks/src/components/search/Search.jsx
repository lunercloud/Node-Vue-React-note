import React, { Component } from 'react'
import { SearchWrapper, SearchInnerWrapper } from './StyledSearch'
import { Icon } from 'antd-mobile'

function Search(props) {

  return (
    <SearchWrapper {...props}>
      <SearchInnerWrapper { ...props }>
        <Icon type="search" size="xs" color="#ee742f">想吃什么搜这里，比如：川菜</Icon>
        <span>想吃什么搜这里，比如：川菜</span>
      </SearchInnerWrapper>
    </SearchWrapper>
  )
}

export default Search;