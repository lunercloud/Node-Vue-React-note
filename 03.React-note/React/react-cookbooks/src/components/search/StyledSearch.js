import styled from 'styled-components'
import border from '@/assets/styled/border'

const SearchWrapper = styled.div `
  height: .65rem;
  padding: .1rem .15rem;
  background-color: ${props => props.outerBg};
`

const SearchInnerWrapper = border(
  styled.div `
    border: ${props => props.border ? 'solid 1px #ee742f' : 0};
    border-radius: .06rem;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: ${props => props.innerBg};
    span {
      color: #666;
      margin-left: .03rem;
    }
  `
)

export {
  SearchWrapper,
  SearchInnerWrapper
}