import React, { Component } from 'react';
import { DetailWrapper } from './StyledDetail'
import NavBar from './NavBar'
import { withRouter } from 'react-router-dom'
import withAnimation from '@/hoc/withAnimation'

@withRouter
@withAnimation
class Detail extends Component {
  handleLeftClick = () => {
    let {history} = this.props
    history.goBack()
  }

  render() {
    const state = this.props.location.state
    return (
      <DetailWrapper>
        <NavBar>
          {state && state.title}
        </NavBar>
        <ul>
          <li>item</li>
          <li>item</li>
          <li>item</li>
        </ul>
      </DetailWrapper>
    )
  }
}

export default Detail