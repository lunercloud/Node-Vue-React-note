import styled from 'styled-components'

const DetailWrapper = styled.div `
  .am-navbar-light {
    background-color: #ee742f;
  }
  .am-icon-md {
    color: #fff;
  }
`

const NavBarWrapper = styled.div `
  height: .44rem;
  background-color: #ee742f;
  color: #fff;
  display: flex;
  align-items: center;
  > div:first-child {
    width: .4rem;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  > div:nth-child(2) {
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    height: .3rem;
    border-radius: .05rem;
    .am-icon-xs {
      width: .3rem;
    }
  }
`

export {
  DetailWrapper,
  NavBarWrapper
}