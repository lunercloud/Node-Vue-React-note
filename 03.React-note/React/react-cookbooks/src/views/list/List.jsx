import React, { Component } from 'react';
import { ListWrapper } from './StyledList'
import NavBar from './NavBar'
import { withRouter } from 'react-router-dom'
import withAnimation from '@/hoc/withAnimation'

@withRouter
@withAnimation
class List extends Component {
  handleLeftClick = () => {
    let {history} = this.props
    history.goBack()
  }

  render() {
    let state = this.props.location.state
    return (
      <ListWrapper>
        <NavBar>
          {state && state.title}
        </NavBar>
        <ul>
          <li>item</li>
          <li>item</li>
          <li>item</li>
        </ul>
      </ListWrapper>
    )
  }
}

export default List