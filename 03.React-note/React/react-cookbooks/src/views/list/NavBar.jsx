import React, { Component } from 'react'
import { NavBarWrapper } from './StyledList'
import { Icon } from 'antd-mobile'
import { withRouter } from 'react-router-dom'

@withRouter
class NavBar extends Component {

  handleClick = () => {
    this.props.history.goBack()
  }

  render() {
    return (
      <NavBarWrapper>
        <div>
          <Icon onClick={this.handleClick} type="left" size="lg" />
        </div>
        <div>
          <Icon type="search" size="xs" />
          {this.props.children}
        </div>
        <div>
          搜索
        </div>
      </NavBarWrapper>
    );
  }
}

export default NavBar;