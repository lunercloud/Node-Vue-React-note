import styled from 'styled-components'

const ListWrapper = styled.div `
  .am-navbar-light {
    background-color: #ee742f;
  }
  .am-icon-md {
    color: #fff;
  }
`

const NavBarWrapper = styled.div `
  height: .44rem;
  background-color: #ee742f;
  color: #fff;
  display: flex;
  align-items: center;
  > div:first-child {
    width: .4rem;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  > div:nth-child(2) {
    flex: 1;
    display: flex;
    align-items: center;
    height: .3rem;
    border-radius: .05rem;
    background-color: #d55f27;
    .am-icon-xs {
      width: .3rem;
    }
  }
  > div:last-child {
    padding-left: .1rem;
    line-height: .44rem;
    width: .6rem;
  }
`

export {
  ListWrapper,
  NavBarWrapper
}