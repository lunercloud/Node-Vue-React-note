import styled from 'styled-components'
import border from '@/assets/styled/border'

// import Search from '@/components/search/Search'

const CategoryWrapper = styled.div `
  height: 100%;
  display: flex;
  overflow: hidden;
  flex-direction: column;
  header {
    height: .44rem;
    background-color: #ee742f;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`

const SliderWrapper = border(
  styled.ul `
    display: flex;
    height: .3rem;
    width: 1.4rem;
    position: relative;
    li {
      flex: 1;
      text-align: center;
      line-height: .3rem;
      position: relative;
      z-index: 1;
      color: #fff;
      &.active {
        color: #ee742f;
      }
      &:last-child {
        position: absolute;
        z-index: 0;
        left: 0;
        top: 0;
        width: 0.7rem;
        height: .3rem;
        background-color: #fff;
        border-radius: .15rem;
        transition: all 200ms ease-in-out;
        &.right {
          left: .7rem;
        }
      }
    }
  `
)

// const SearchWrapper = border(
//   styled.Search `

//   `
// )

export {
  CategoryWrapper,
  SliderWrapper,
  // SearchWrapper
}