import { CHANGE } from './actionTypes'

export const change = tab => {
  return {
    type: CHANGE,
    tab
  }
}