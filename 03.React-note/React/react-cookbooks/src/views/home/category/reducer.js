import { Map } from 'immutable'
import { CHANGE } from './actionTypes'

const defaultState = Map({
  tab: '热门'
})

const reducer = (state = defaultState, action) => {
  switch(action.type) {
    case CHANGE:
      return state.set('tab', action.tab)
    
    default:
      return state
  }
}

export default reducer