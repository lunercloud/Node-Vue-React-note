import React, { useState, useCallback } from 'react'
import { TabBar } from 'antd-mobile'

import cookbook from '@/assets/images/cookbook.png'
import cookbookActive from '@/assets/images/cookbook-active.png'
import menu from '@/assets/images/menu.png'
import menuActive from '@/assets/images/menu-active.png'
import location from '@/assets/images/location.png'
import locationActive from '@/assets/images/location-active.png'
import more from '@/assets/images/more.png'
import moreActive from '@/assets/images/more-active.png'

import { Cookbook } from './cookbook'
import { Category } from './category'
import { useSelector } from 'react-redux'
import { Map } from './map'
import { More } from './more'
import withAnimation from '@/hoc/withAnimation'

const Home = withAnimation(
  function () {
    const [state, setState] = useState({
      selectedTab: 'more',
      hidden: false,
      fullScreen: true
    })
  
    const show = useSelector(
      state => {
        return state.getIn(['more', 'show'])
      }
    )
    
    const handleSwitchTab = useCallback(
      (tab) => {
        return () => {
          setState({
            ...state,
            selectedTab: tab
          })
        }
      },
      [state]
    )
  
    const tabItems = [
      <TabBar.Item
        title="美食大全"
        key="cookbook"
        icon={<div style={{
          width: '22px',
          height: '22px',
          background: `url(${cookbook}) center center /  26px 26px no-repeat` }}
        />
        }
        selectedIcon={<div style={{
          width: '22px',
          height: '22px',
          background: `url(${cookbookActive}) center center /  26px 26px no-repeat` }}
        />
        }
        selected={state.selectedTab === 'cookbook'}
        onPress={handleSwitchTab('cookbook')}
        data-seed="logId"
      >
        <Cookbook></Cookbook>
      </TabBar.Item>,
      <TabBar.Item
        icon={
          <div style={{
            width: '22px',
            height: '22px',
            background: `url(${menu}) center center /  18px 18px no-repeat` }}
          />
        }
        selectedIcon={
          <div style={{
            width: '22px',
            height: '22px',
            background: `url(${menuActive}) center center /  18px 18px no-repeat` }}
          />
        }
        title="分类"
        key="category"
        selected={state.selectedTab === 'category'}
        onPress={handleSwitchTab('category')}
      >
        <Category></Category>
      </TabBar.Item>,
      <TabBar.Item
        icon={
          <div style={{
            width: '22px',
            height: '22px',
            background: `url(${location}) center center /  21px 21px no-repeat` }}
          />
        }
        selectedIcon={
          <div style={{
            width: '22px',
            height: '22px',
            background: `url(${locationActive}) center center /  21px 21px no-repeat` }}
          />
        }
        title="美食地图"
        key="map"
        selected={state.selectedTab === 'map'}
        onPress={handleSwitchTab('map')}
      >
        <Map></Map>
      </TabBar.Item>,
      <TabBar.Item
        icon={{ uri: more }}
        selectedIcon={{ uri: moreActive }}
        title="更多"
        key="more"
        selected={state.selectedTab === 'more'}
        onPress={handleSwitchTab('more')}
      >
        <More></More>
      </TabBar.Item>
    ]
  
    if (!show) {
      tabItems.splice(2, 1)
    }
  
    return (
      <div style={state.fullScreen ? { position: 'fixed', height: '100%', width: '100%', top: 0 } : { height: 400 }}>
        <TabBar
          unselectedTintColor="#949494"
          tintColor="#000"
          barTintColor="white"
          hidden={state.hidden}
        >
          { tabItems }
        </TabBar>
      </div>
    )
  }
)

export default Home