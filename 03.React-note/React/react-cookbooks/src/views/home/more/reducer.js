import { CHANGE } from './actionTypes'
import { Map } from 'immutable'

const getShow = () => {
  let show = localStorage.getItem('show')
  if (show === null) {
    return true
  }
  return JSON.parse(show)
}

const defaultState = Map({
  show: getShow()
})

const reducer = (state = defaultState, action) => {
  switch(action.type) {
    case CHANGE:
      return state.set('show', !state.get('show'))

    default: 
      return state
  }
}

export default reducer