import { CHANGE } from './actionTypes'

export const change = () => {
  return {
    type: CHANGE
  }
}