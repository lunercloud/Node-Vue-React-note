import styled from 'styled-components'

const MoreWrapper = styled.div `
  .am-navbar {
    background-color: #ee742f;
  }

  .main {
    padding: .2rem;
  }

  img {
    width: 2rem;
    height: 1.5rem;
    border: solid 1px #ccc;
  }

  .weixin {
    padding: .2rem;
  }
`

export {
  MoreWrapper
}