import React, { useState, useEffect } from 'react'

import * as actions from '../actionCreator.js'
import { useActions } from '@/hooks/useActions'
import { useSelector } from 'react-redux'
import { NavBar, Switch } from 'antd-mobile'
import { MoreWrapper } from './StyledMore'

function More() {
  const [ imgsrc, setImgsrc ] = useState('')

  const show = useSelector(
    state => state.getIn(['more', 'show'])
  )

  const actionObj = useActions(actions)

  const handleChange = () => {
    actionObj.change()
    localStorage.setItem('show', !show)
  }

  const handleClick = () => {
    console.log()
    window.wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
        setImgsrc(localIds)
      }
    });
  }

  // useEffect(() => {
  //   window.wx.ready(function(){
  //     window.wx.chooseImage({
  //       count: 1, // 默认9
  //       sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
  //       sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
  //       success: function (res) {
  //         var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
  //         setImgsrc(localIds)
  //       }
  //     });
  //   });
  // }, [])
 
  return (
    <MoreWrapper>
      <NavBar
        mode="dark"
      >
        更多
      </NavBar>
      <div className="main">
        显示地图：
        <Switch
          checked={show}
          onChange={handleChange}
        />
      </div>
      <div className="weixin">
        <div>
          <button onClick={handleClick}>拍照</button>
        </div>
        <div>
          <img src={imgsrc} alt=""/>
        </div>
      </div>
    </MoreWrapper>
  );
}

export default More;