import More from './container/More'
import reducer from './reducer'

export {
  More,
  reducer
}