import React from 'react'
import { NavBar } from 'antd-mobile'

import { MapWrapper } from './StyledMap'

function Map() {
  return (
    <MapWrapper style={{height: '100%'}}>
      <NavBar
        mode="dark"
      >美食地图</NavBar>

      <iframe
        src="/map.html" 
        frameBorder="0"
        title="ifrm"
        style={{width: '100%', height: '100%'}}
      ></iframe>
    </MapWrapper>
  );
}

export default Map;