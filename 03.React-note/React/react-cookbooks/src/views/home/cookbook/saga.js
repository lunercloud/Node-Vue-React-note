import { call, put, takeEvery } from 'redux-saga/effects'
import axios from 'axios'

import { loadListDataSync, loadHotCateDataSync } from './actionCreator'

import { LOADHOTCATEDATASAGA, LOADLISTDATASAGA } from './actionTypes'

const fetchData = (url) => {
  return axios({
    url
  }).then(result => {
    return result
  })
}

const LoadListDataAsync = function* () {
  let result = yield call(fetchData, '/api/list')
  yield put(
    loadListDataSync(result.data.data)
  )
}

const LoadHotCateDataAsync = function* () {
  let result = yield call(axios, {
    url: '/api/hotcat'
  })
  yield put(
    loadHotCateDataSync(result.data)
  )
}

function* saga() {
  yield takeEvery(LOADLISTDATASAGA, LoadListDataAsync)
  yield takeEvery(LOADHOTCATEDATASAGA, LoadHotCateDataAsync)
}

export default saga