import React from 'react'

import Swiper from './Swiper'
import Search from '@/components/search/Search'
import HotCate from './hotCate'
import Top10 from './Top10'

import {
  CookbookWrapper
} from './StyledCookbook'

export default function CookbookUi(props) {
  return (
    <CookbookWrapper>
      <header>美食大全</header>
      <Swiper
        list={props.list}
      ></Swiper>
      <Search
        borderWidth="1px"
        borderColor="#ee742f"
        borderRadius={0.06}
        innerBg="#fff"
        outerBg="#f6f6f9"
      ></Search>
      <HotCate
        hotCate={props.hotCate}
      ></HotCate>
      <Top10
        list={props.list.slice(0, 10)}
      ></Top10>
    </CookbookWrapper>
  )
}
