import styled from 'styled-components'
import border from '@/assets/styled/border'

// const h3 = `
//   h3 {
//     height: .44rem;
//     background-color: #fff;
//     padding-left: .15rem;
//     line-height: .44rem;
//     font-weight: normal;
//     color: #666;
//     border-bottom: solid 1px #ccc;
//   }
// `

const CookbookWrapper = styled.div `
  header {
    height: .44rem;
    background-color: #ee742f;
    color: #fff;
    text-align: center;
    line-height: .44rem;
    font-size: .18rem;
  }
  `

const SwiperWrapper = styled.div `
  position: relative;
  height: 0;
  padding-bottom: 66.6666667%;
  img {
    width: 100%;
  }
  .swiper-pagination {
    position: absolute;
    bottom: .1rem;
    left: 50%;
    transform: translate(-50%, 0)
  }
`

const H3 = border(
  styled.h3 `
    height: .44rem;
    background-color: #fff;
    padding-left: .15rem;
    line-height: .44rem;
    font-weight: normal;
    color: #666;
  `
)

const HotcateWrapper = styled.div `
  img {
    border-radius: .1rem;
  }
  .am-grid {
    padding-bottom: .1rem;
    background-color: #fff;
  }
  img[src=none] {
    visibility: hidden;
    height: .3rem !important;
  }
  
`

const Top10Wrapper = styled.div `
  h3 {
    background-color: #f6f6f9;
    height: .5rem;
    padding-top: .06rem;
  }
  ul {
    display: flex;
    flex-wrap: wrap;
    padding-left: .1rem;
    li {
      width: 50%;
      padding-right: .1rem;
      > div:first-child {
        img {
          width: 100%;
        }
      }
      > div:last-child {
        height: .6rem;
        background-color: #fff;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin-bottom: .1rem;
        h1 {
          font-size: .18rem;
        }
        h2 {
          font-weight: normal;
          color: #666;
        }
      }
    }
  }
`

export {
  CookbookWrapper,
  SwiperWrapper,
  HotcateWrapper,
  Top10Wrapper,
  H3
}