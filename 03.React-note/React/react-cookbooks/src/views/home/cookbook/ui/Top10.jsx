import React from 'react'
import { Top10Wrapper, H3 } from './StyledCookbook'
import { useHistory } from 'react-router-dom'

function Top10(props) {
  const history = useHistory()

  const handleClick = (title) => {
    return () => {
      history.push('/detail', { title })
    }
  }

  return (
    <Top10Wrapper>
      <H3>精品好菜</H3>
      <ul>
        {
          props.list.map(item => (
            <li 
              onClick={handleClick(item.name)}
              key={item.id}
            >
              <div>
                <img src={item.img} alt=""/>
              </div>
              <div>
                <h1>{item.name}</h1>
                <h2>{item.all_click}浏览 {item.favorites}收藏</h2>
              </div>
            </li>
          ))
        }
      </ul>
    </Top10Wrapper>
  );
}

export default Top10