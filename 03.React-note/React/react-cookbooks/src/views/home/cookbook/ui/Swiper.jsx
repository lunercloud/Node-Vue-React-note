import React, { useState, useEffect } from 'react'
import Swiper from 'swiper'

import { SwiperWrapper } from './StyledCookbook'
import { useUpdate } from '@/hooks/useUpdate' 

let swiper = null

function Carousel(props) {
  // console.log(props)
  // let [list] = useState(props.list)

  useEffect(() => {
    swiper = new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      }
    })
  }, [])

  useUpdate(() => {
    swiper.update()
  })

  return (
    <SwiperWrapper className="swiper-container">
      <div className="swiper-wrapper">
        {
          props.list.slice(0, 5).map(item => {              
            return (
              <div className="swiper-slide" key={item.id}>
                <img src={item.img} alt=""/>
              </div>
            )
          })
        }
      </div>
      <div className="swiper-pagination"></div>
    </SwiperWrapper>
  )
}

export default Carousel;