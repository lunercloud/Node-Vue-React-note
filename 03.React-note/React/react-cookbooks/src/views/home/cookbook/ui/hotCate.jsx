import React, { useMemo } from 'react'
import { Grid } from 'antd-mobile'
import { useHistory } from 'react-router-dom'

import { HotcateWrapper, H3 } from './StyledCookbook'

function HotCate(props) {
  const history = useHistory()
  
  const filterHotCate = useMemo(() => {
    return [
      ...props.hotCate,
      {
        img: 'none',
        title: '更多...'
      }
    ]
  }, [props.hotCate])

  const handleItemClick = ({ title }) => {
    history.push('/list', { title })
  }

  return (
    <HotcateWrapper>
      <H3
        borderWidth="0 0 1px 0"
        borderStyle="solid"
        borderColor="#ccc"
        borderRadius={0}
      >热门分类</H3>
      <Grid 
        data={filterHotCate}
        columnNum={3}
        hasLine={false}
        onClick={handleItemClick}
        renderItem={dataItem => (
          <div>
            <img src={dataItem.img} style={{ width: '75px', height: '75px' }} alt="" />
            <div style={{ color: '#888', fontSize: '14px', marginTop: '12px' }}>
              <span>{dataItem.title}</span>
            </div>
          </div>
        )}
      />
    </HotcateWrapper>
  );
}

export default HotCate