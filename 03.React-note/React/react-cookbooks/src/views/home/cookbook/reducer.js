import { LOADHOTCATEDATA, LOADLISTDATA } from './actionTypes'
import { Map, List } from 'immutable'

const defaultState = Map({
  list: List([]),
  hotCate: List([])
})

const reducer = (state = defaultState, action) => {
  switch(action.type) {
    case LOADLISTDATA:
      return state.set('list', List(action.list))

    case LOADHOTCATEDATA:
      return state.set('hotCate', List(action.hotCate))

    default:
      return state
  }
}

export default reducer