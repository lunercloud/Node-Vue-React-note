import { 
  LOADHOTCATEDATA, 
  LOADLISTDATA,
  LOADHOTCATEDATASAGA, 
  LOADLISTDATASAGA,
 } from './actionTypes'

export const loadListDataSync = (list) => {
  return {
    type: LOADLISTDATA,
    list
  }
}

export const loadHotCateDataSync = (hotCate) => {
  return {
    type: LOADHOTCATEDATA,
    hotCate
  }
}

export const loadListDataSyncSaga = (list) => {
  return {
    type: LOADLISTDATASAGA,
    list
  }
}

export const loadHotCateDataSyncSaga = (hotCate) => {
  return {
    type: LOADHOTCATEDATASAGA,
    hotCate
  }
}