import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useActions } from '@/hooks/useActions'
import CookbookUi from '../ui/CookbookUi'
import * as actions from '../actionCreator'

function Cookbook() {

  const state = useSelector(
    state => {
      return ({
        list: state.getIn(['home', 'list']),
        hotCate: state.getIn(['home', 'hotCate'])
      })
    }
  )

  const actionsObj = useActions(actions)

  // const dispatch = useDispatch()
  
  useEffect(() => {
    actionsObj.loadListDataSyncSaga()
    actionsObj.loadHotCateDataSyncSaga()
    // dispatch(actions.loadListDataSyncSaga())
  }, [actionsObj])

  return (
    <CookbookUi
      list={state.list}
      hotCate={state.hotCate}
    ></CookbookUi>
  )
}

export default Cookbook