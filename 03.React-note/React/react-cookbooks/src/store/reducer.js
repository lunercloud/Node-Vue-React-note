import { combineReducers } from 'redux-immutable'
import { reducer as home } from '../views/home/cookbook'
import { reducer as more } from '../views/home/more'
import { reducer as cate } from '../views/home/category'

export default combineReducers({
  home,
  more,
  cate
})