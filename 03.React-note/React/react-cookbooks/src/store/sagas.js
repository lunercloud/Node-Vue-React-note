import { saga as cookbookSaga } from '../views/home/cookbook'

const sagas = [
  cookbookSaga
]

export default sagas