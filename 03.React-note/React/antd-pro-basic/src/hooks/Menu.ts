import { useState, useEffect } from 'react'

const useMenu = () => {
  const [menuData, setMenuData] = useState([]);

  useEffect(() => {
    // 这里是一个演示用法
    // 真实项目中建议使用 dva dispatch 或者 umi-request
    fetch('/api/menu')
      .then(response => response.json())
      .then(data => {
        setMenuData(data || []);
      });
  }, []);

  return {
    menuData
  }
}

export default useMenu