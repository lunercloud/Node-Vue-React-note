import { Request, Response } from 'express'
export default {
  '/api/menu': (req: Request, res: Response) => {
    res.send(
      [
        {
          "path": "/dashboard",
          "name": "dashboard",
          "icon": "dashboard",
          "children": [
            {
              "path": "/dashboard/analysis",
              "name": "analysis"
            },
            {
              "path": "/dashboard/monitor",
              "name": "monitor"
            },
            {
              "path": "/dashboard/workplace",
              "name": "workplace"
            }
          ]
        }
      ]
    )
  },
};