import React, { Component } from 'react'
import Child21 from './Child2-1'
import { colorContext } from './context/colorContext'

class Child2 extends Component {
  static contextType = colorContext
  render() {
    return (
      <div>
        <h1>Child2</h1>
        <Child21></Child21>
      </div>
    )
  }

  componentDidMount() {
    console.log(this.context)
  }
}

export default Child2;