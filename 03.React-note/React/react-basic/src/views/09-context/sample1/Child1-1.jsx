import React, { Component } from 'react'
import { Consumer as ListConsumer } from './context/listContext'
import { Consumer as ColorConsumer } from './context/colorContext'

class Child11 extends Component {
  // static contextType = listContext

  render() {
    return (
      <div>
        {/* {JSON.stringify(this.context)} */}

        <ColorConsumer>
          {
            (color) => {
              return (
                <>
                  <div>{color.color}</div>
                  <ListConsumer>
                    {
                      (list) => {
                        return JSON.stringify(list)
                      }
                    }
                  </ListConsumer>
                </>
              )
            }
          }
        </ColorConsumer>
      </div>
    );
  }

  componentDidMount() {
    console.log(this)
    
  }
}

export default Child11;