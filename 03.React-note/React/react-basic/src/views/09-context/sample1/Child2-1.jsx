import React from 'react'
import { Consumer } from './context/listContext'

function Child21(props) {
  console.log(props)
  return (
    <div>
      <Consumer>
        {
          (value) => {
            return JSON.stringify(value)
          }
        }
      </Consumer>
    </div>
  )
}

export default  Child21