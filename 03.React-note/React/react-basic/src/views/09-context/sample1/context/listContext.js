import { createContext } from 'react'

const listContext = createContext(['x', 'y', 'z'])

const { Provider, Consumer } = listContext

export {
  Provider,
  Consumer,
  listContext
}