import { createContext } from 'react'

const colorContext = createContext()

const { Provider, Consumer } = colorContext

export {
  Provider,
  Consumer,
  colorContext
}