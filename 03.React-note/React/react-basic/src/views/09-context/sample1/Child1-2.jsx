import React, { Component } from 'react'
import { Consumer } from './context/listContext'

class Child12 extends Component {
  render() {
    return (
      <div>
        <Consumer>
          {
            (value) => {
              return JSON.stringify(value)
            }
          }
        </Consumer>
      </div>
    )
  }
}

export default Child12;