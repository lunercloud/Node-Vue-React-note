import React, { Component } from 'react'
import Child1 from './Child1'
import Child2 from './Child2'
import { Provider } from './context/colorContext'

class Context extends Component {
  render() {
    return (
      <div>
        <Provider value={{color: 'red'}}>
          <Child1></Child1>
        </Provider>
        <Provider value={{color: 'green'}}>
          <Child2></Child2>
        </Provider>
      </div>
    );
  }
}

export default Context;