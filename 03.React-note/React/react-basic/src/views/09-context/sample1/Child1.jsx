import React, { Component } from 'react'
import Child11 from './Child1-1'
import Child12 from './Child1-2'

class Child1 extends Component {
  render() {
    return (
      <div>
        <h1>Child1</h1>
        <Child11></Child11>
        <Child12></Child12>
      </div>
    );
  }
}

export default Child1;