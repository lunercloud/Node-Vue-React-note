import React, { Component } from 'react'
import { CounterConsumer } from './counterContext'

class Child extends Component {
  render() {
    return (
      <div>
        <CounterConsumer>
          {
            (value) => {
              return (
                <div>{value.count}</div>
              )
            }
          }
        </CounterConsumer>
      </div>
    );
  }
}

export default Child;