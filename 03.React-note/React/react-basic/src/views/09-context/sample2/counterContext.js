import React, {Component, createContext } from 'react'

const { Provider, Consumer: CounterConsumer } = createContext()

class CounterProvider extends Component {
  state = {
    count: 0
  }

  add = () => {
    this.setState(state => ({
      count: state.count + 1
    }))
  }

  minus = () => {
    this.setState(state => ({
      count: state.count - 1
    }))
  }

  render() {
    return (
      <Provider
        value={{
          count: this.state.count,
          add: this.add,
          minus: this.minus
        }}
      >
        {this.props.children}
      </Provider>
    )
  }
}

export {
  CounterProvider,
  CounterConsumer
}

// <CounterProvider>
//   <App></App>
// </CounterProvider>