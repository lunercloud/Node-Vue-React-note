import React, { Component } from 'react'
import { CounterConsumer } from './counterContext'
import Child from './Child'

class Counter extends Component {
  render() {
    return (
      <div>
        <CounterConsumer>
          {
            (value) => {
              return (
                <>
                  <div>{value.count}</div>
                  <div>
                    <button onClick={value.add}>add</button>
                  </div>
                </>
              )
            }
          }
        </CounterConsumer>
        <Child></Child>
      </div>
    )
  }
}

export default Counter;