import React, { Fragment, memo } from 'react'

function TodoListUl(props) {  
  console.log(props)
  return (
    <ul>
        {
          props.list.map((li, i) => {            
            return (
              <Fragment key={li.name + i}>
                {
                  (li.isFinished === props.finished) && (
                    <li>
                      <input 
                        type="checkbox" 
                        defaultChecked={li.isFinished} 
                        onClick={props.handleCheckboxClick(li.isFinished, i)}
                      />
                      {li.name}
                      <button onClick={props.handleClick(i)}>x</button>
                    </li>
                  )
                }
              </Fragment>
            )
          })
        }
      </ul>
  )
}


export default memo(TodoListUl)