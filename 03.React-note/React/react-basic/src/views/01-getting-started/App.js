import React, { Component} from 'react'
import Child from './Child'

class App extends Component {
  render() {
    return (
      <Child title="hello">
        <div>hello react</div>
      </Child>
    )
  }
}

export default App