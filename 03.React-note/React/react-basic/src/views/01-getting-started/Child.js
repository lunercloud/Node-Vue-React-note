function Child(props) {
  let { title, children } = props
  return (
    <>
      <div>child - {title}</div>
      {
        children
      }
    </>
  )
}

export default Child