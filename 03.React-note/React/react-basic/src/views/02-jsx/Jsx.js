import React, { Component, createElement } from 'react'

class Jsx extends Component {
  render() {
    return (
      <>
        {
          createElement(
            'div',
            {
              className: 'abc',
              id: 'appRoot'
            },
            createElement(
              'h1',
              {
                className: 'title'
              },
              '欢迎进入React的世界'
            ),
            createElement(
              'h1',
              {
                className: 'title'
              },
              '欢迎进入React的世界'
            )
          )
        }
      </>
    )
  }
}

export default Jsx