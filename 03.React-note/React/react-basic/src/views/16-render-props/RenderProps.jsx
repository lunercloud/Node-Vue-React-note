import React from 'react'
import Child from './Child'

export default function RenderProps() {
  return (
    <Child
      render={
        () => {
          return (
            <div>contents</div>
          )
        }
      }
    >
      {/* {
        () => {
          return (
            <div>contents</div>
          )
        }
      } */}
      <div>hello</div>
      <div>world</div>
    </Child>
  )
}
