import React, { Children } from 'react'

export default function Child(props) {
  // const renderContent = props.children
  return (
    <div>
      child
      {/* {props.children} */}
      {props.render()}


      {
        Children.map(props.children, (div) => {
          console.log(div.props.children)
        })
      }
    </div>
  )
}
