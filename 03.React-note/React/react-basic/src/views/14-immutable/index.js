// import { cloneDeep } from 'lodash'

// const state = {
//   str: '千锋教育',
//   obj: {
//     y: 1
//   },
//   arr: [1, 2, 3]
// }
// const newState = cloneDeep(state)

// newState.obj.y = 2
// newState.arr.push(4)

// console.log(state, newState)


const { Map, is, List, Seq, fromJS } = require('immutable')

// const map = Map({
//   x: 100,
//   y: 200,
//   z: 300
// })

// const map2 = map.set('x', 200)

// console.log(map.get('x'), map2.get('x'))

// const obj1 = {
//   x: 1
// }

// const obj2 = {
//   x: 1
// }

// console.log(obj1 === obj2)
// console.log(JSON.stringify(obj1) === JSON.stringify(obj2))

// const map1 = Map({
//   x: 0,
//   y: 1,
//   z: 2
// })

// const map2 = Map({
//   x: 0,
//   y: 1,
//   z: 2
// })

// const map3 = map2
// const map4 = map3.set('x', 100)

// console.log(map3 === map2)
// console.log(map2.get('x'), map4.get('x'))

// console.log(map1 === map2)
// console.log(map1.equals(map2))
// console.log(is(map1, map2))

// const list1 = List(['a', 'b', 'c'])
// const list2 = list1.push('d')

// console.log(list2)

// console.log(list1.concat(['e']))
// console.log(['e'].concat(list2))
// console.log(list2.size)

// const map1 = Map({
//   x: 100,
//   y: 200
// })

// const map2 = map1.map((v, k) => {
//   return {
//     [v]: k
//   }
// })

// console.log(map2.toJS())

// const alpha = Map({ a: 1, b: 2, c: 3, d: 4 })
// const upperCase = alpha.map((v, k) => k.toUpperCase())

// console.log(upperCase.join(''))


// const map1 = Map({ a: 1, b: 2, c: 3, d: 4 });
// const map2 = Map({ c: 10, a: 20, t: 30 });

// const map3 = map1.merge(map2)
// console.log(map3.toObject())

// const obj = { d: 100, o: 200, g: 300 }
// const map3 = map1.merge(map2, obj)

// console.log(map3.toJS())

// const myObject = {a: 1, b: 2, c: 3}
// const myArray = [100, 200, 300]

// const seq = Seq(myObject)
// const seq2 = Seq(myArray)

// console.log(seq.map(v => v * v).toJS())
// console.log(seq2.map(v => v * v).toJS())

// const arr = [ 1, 2, 3, 4, 5, 6, 7, 8 ]
// const seq = Seq(arr)

// const arr2 = arr
//   .filter((v) => {
//     console.log(v)
//     return v % 2 !== 0
//   })
//   .map((v) => {
//     console.log(v)
//     return v * v
//   })

// console.log(arr2)

// const seq2 = seq
//   .filter(v => {
//     console.log('f:', v)
//     return v % 2 !== 0
//   })
//   .map(v => {
//     console.log('m:', v)
//     return v * v
//   })

// console.log(seq2)
// console.log(seq2.toJS())

// console.log(test)
// var test = false
// var year = 2013
// function test() {
//   console.log(0)
// }


// const map = Map({ a: 1, b: 2, c: 3 });
// const lazySeq = Seq(map);
// const newMap = lazySeq
//   .flip()
//   .map(key => key.toUpperCase())
//   .flip();

// console.log(newMap.toJS())

// const { Range } = require('immutable')

// const aRange = Range(1, Infinity)
//   .skip(1000)
//   .map(n => -n)
//   .filter(n => n % 2 === 0)
//   .take(2)
//   .reduce((r, n) => r * n, 1);
  
// console.log(aRange)

const obj = [{
  x: 100,
  y: 200,
  z: {
    a: 100,
    b: [{
      w: 1000
    }]
  }
}]

const map = fromJS(obj)

// console.log(obj[0].z.b[0].w)

// console.log(map)
// console.log(map.get(0).get('z').get('b').get(0).get('w'))
// console.log(map.getIn([0, 'z', 'b', 0, 'w']))
// const map2 = map.setIn([0, 'z', 'b', 0, 'w'], 2000)
// const map2 = map.updateIn([0, 'z', 'b', 0, 'w'], (w) => {
//   return w * w
// })
// console.log(JSON.stringify(map2.toJS()))

const map2 = map.updateIn([0, 'z', 'b', 0, 'w'], (w) => {
  return w * w
})

const map3 = map.withMutations((map) => {
  map.setIn([0, 'z', 'b', 0, 'w'], 2000)
})

console.log(JSON.stringify(map3.toJS()))