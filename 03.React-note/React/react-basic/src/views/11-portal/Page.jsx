import React, { Component } from 'react'
import Dialog from './Dialog'

// function Test(props) {
//   return (
//     <div>
//       test
//       {props.children}
//     </div>
//   )
// }

class Page extends Component {
  state = {
    show: false
  }

  handleOpenDialog = (e) => {
    e.stopPropagation()
    this.setState({
      show: true
    })
  }

  handleCloseDialog = () => {
    this.setState({
      show: false
    })
  }

  render() {
    return (
      <div onDoubleClick={this.handleCloseDialog}>
        <button onClick={this.handleOpenDialog}>打开</button>
        {
          this.state.show && (
            // <Test 
            //   onClick={() => { console.log(1000) }} 
            //   onCloseDialog={() => { console.log(1) }}
            // >
            //   <Dialog
            //     onClick={() => { console.log(100) }} 
            //     onCloseDialog={() => { console.log(0) }}
            //   ></Dialog>
            // </Test>
            <Dialog></Dialog>
          )
        }
      </div>
    );
  }
}

// $('div').on('onMyEvent', () => {
  
// })

// $('div').trigger('onMyEvent')

export default Page;