import React, { Component } from 'react'
import './style.css'
import withPortal from './withPortal'

@withPortal
class Dialog extends Component {
  render() {
    return (
      <div className="dialog">
        {/* <button
          onClick={this.props.onCloseDialog}
        >关闭</button> */}
        {/* 点击关闭，会将click事件冒泡父组件上绑定click的元素上 */}
        {/* 这里是不能调用父组件里传递过来的自定义事件 */}
        <button>关闭</button>
      </div>
    )
  }
}

export default Dialog;