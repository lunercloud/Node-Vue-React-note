import React from 'react'
import ReactDOM from 'react-dom'

// 将组建渲染到和 <div id="root"> 平级的位置
function withPortal(Comp) {
  return class extends React.Component {
    render() {
      return (
        // 返回一个新的组件的实例, 返回的是一个组件树
        ReactDOM.createPortal(
          <Comp></Comp>,
          document.body
        )
      )
    }
  }
}

export default withPortal