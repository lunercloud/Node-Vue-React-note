import React, { Component } from 'react';
import classNames from 'classnames'
import './style.css'

import { Container } from './Styled'

class Styles extends Component {
  state = {
    style: 'font'
  }

  render() {
    var btnClass = classNames({
      font: true,
      'btn-pressed': this.state.isPressed,
      'btn-over': !this.state.isPressed && this.state.isHovered
    });
    return (
      <div className={btnClass}>
        hello world
        <Container> !!!!!! </Container>
      </div>
    );
  }
}

export default Styles;