import React, { Component } from 'react';

class Form extends Component {
  constructor(props) {
    super(props)
    this.state = {
      inputValue: '',
      textareaValue: '',
      selectValue: ['mango', 'coconut']
    }
  }

  handleChange = (e) => {
    this.setState({
      inputValue: e.target.value
    })
  }

  handleTextAreaChange = (e) => {
    this.setState({
      textareaValue: e.target.value
    })
  }

  handleSelectChange = (e) => {
    this.setState({
      selectValue: e.target.value
    })
  }

  render() {
    return (
      <>
        <div>
          <input 
            type="text"
            value={this.state.inputValue}
            onChange={this.handleChange}
          />
          {this.state.inputValue}
        </div>
        <div>
          <textarea cols="30" rows="10"
            value={this.state.textareaValue}
            onChange={this.handleTextAreaChange}
          ></textarea>
          {this.state.textareaValue}
        </div>
        <div>
        <select
          value={this.state.selectValue}
          onChange={this.handleSelectChange}
          multiple
        >
          <option value="grapefruit">葡萄柚</option>
          <option value="lime">酸橙</option>
          <option value="coconut">椰子</option>
          <option value="mango">芒果</option>
        </select>
        {this.state.selectValue}
        </div>
      </>
    );
  }
}

export default Form;