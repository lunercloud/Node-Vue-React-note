import React, { Component, createRef } from 'react';

class Form extends Component {
  constructor(props) {
    super(props)
    this.fileRef = createRef()
    this.state = {
      value: null
    }
  }

  // state = {
  //   fileList: ''
  // }

  handleChange = (e) => {
    // this.setState({
    //   fileList: e.target.value
    // })
    // this.setState({
    //   value: e.target.value
    // })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    console.log(this.fileRef.current.files[4].name)
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input 
            type="file"
            ref={this.fileRef}
            multiple
          />
          {/* {this.state.fileList} */}

          <input type="text" value={this.state.value} onChange={this.handleChange}/>

          <input type="submit" />
        </form>
      </div>
    );
  }

  componentDidMount() {
    // this.setState({
    //   fileList: 'aa.gif'
    // })
  }
}

export default Form;