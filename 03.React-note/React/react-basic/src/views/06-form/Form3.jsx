import React, { Component } from 'react';

class Form extends Component {
  state = {
    checkedValue: false,
    numberValue: 10
  }

  handleChange = (e) => {
    let target = e.target
    // console.log(target.name) 
    let name = target.name     
    let value = name === 'checkedValue' ? target.checked : target.value
    
    this.setState({
      [name]: value
    })
  }

  render() {
    return (
      <div>
        <div>
          <input 
            name="checkedValue"
            type="checkbox"
            checked={this.state.checkedValue}
            onChange={this.handleChange}
          />
          {JSON.stringify(this.state.checkedValue)}
        </div>
        <div>
          <input 
            name="numberValue"
            type="number"
            value={this.state.numberValue}
            onChange={this.handleChange}
          />
          {this.state.numberValue}
        </div>
      </div>
    );
  }
}

export default Form;