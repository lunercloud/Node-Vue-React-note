import React, { Component } from 'react'
import Child from './Child'
import Child2 from './Child2'
// import Child3 from './Child3'

class DataRender extends Component {
  state = {
    child2: Child2,
    count: 100,
    list: ['a', 'b', 'c']
  }

  handleClick = () => {
    this.state.list.push('d')
    // console.log(this.state.list)
    // this.setState({})
    // this.forceUpdate()
    
    this.setState({}, () => {
      console.log(document.querySelector('#ul').innerHTML)
    })

  }

  handleClick2 = () => {
    this.setState((preState) => {
      return {
        count: preState.count + 1
      }
    }, () => {
      
    })
  }

  handleClick3 = () => {
    this.setState({})
  }

  // static child2 = Child2

  render() {
    console.log(0)
    
    // const child2 = Child2
    return (
      <div>
        hello
        <Child 
          url="http://www.baidu.com"
          // Title={'hello'}
          // title={['a','b']}
          // title={Symbol(123)}
          // title={<><div>hello</div></>}
          // title={<Child></Child>}
          // title={Child2}
          // title={<Child2 />}
          // title={new Child2()}
          // title="abcabc"
          // title={['a', 100]}
          title={{x: '100', y: 200}}
        >
          <div>slot content</div>
        </Child>
        {/* <Child3></Child3> */}
        { this.state.count }
        <ul id="ul">
          {
            this.state.list.map(value => <li key={value}>{value}</li>)
          }
        </ul>
        <div>
          <button onClick={this.handleClick}>click</button>
        </div>
        <div>
          <button onClick={this.handleClick2}>click2</button>
        </div>
        <div>
          <button onClick={this.handleClick3}>click3</button>
        </div>
      </div>
    );
  }
}

export default DataRender;