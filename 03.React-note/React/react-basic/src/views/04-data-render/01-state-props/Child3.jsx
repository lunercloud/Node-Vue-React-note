import React from 'react'
import { string } from 'prop-types'
// import Child2 from './Child2'

export default function Child3(props) {
  return (
    <div>
      {props.title}
    </div>
  )
}

Child3.defaultProps = {
  title: 'hello world'
}

Child3.propTypes = {
  title: string
}