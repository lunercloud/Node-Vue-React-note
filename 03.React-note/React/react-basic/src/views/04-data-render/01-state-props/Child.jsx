import React, { Component } from 'react'
import Child2 from './Child2'
import { 
  array,
  symbol,
  node,
  bool,
  element, 
  string,
  elementType,
  instanceOf,
  oneOf,
  oneOfType,
  // arrayOf,
  // objectOf,
  // number,
  // shape,
  // exact
} from 'prop-types'

class Child extends Component {
  static defaultProps = {
    url: 'http://qfedu.com'
  }

  static propTypes = {
    // Title: string
    // title: array
    // title: symbol
    // title: node
    // title: element 
    // title: elementType
    // title: instanceOf(Child2)
    // title: oneOf(['abc', 'def'])
    title2: oneOfType([
      string,
      array,
      bool,
      node,
      symbol,
      element,
      elementType,
      oneOf(['abc', 'def']),
      instanceOf(Child2)
    ]),

    // title: arrayOf(string)
    // title: objectOf(number)
    // title: shape({
    //   x: string,
    //   y: number
    // })
    // title: exact({
    //   x: string,
    //   y: number
    // }).isRequired

    title: (props, propName, componentName) => {
      // return new Error('出错了。')
    }
  }

  render() {
    // console.log(this)
    // this.props.url = 'http://qfedu.com'
    // let Title = this.props.title
    // 不能在模板里使用小写字母开头的自定义组件，
    // 但却可以使用引用小写字母的开头props的变量充当组件使用
    
    return (
      <div>
        {/* { this.props.url } */}
        {/* { this.props.children } */}
        {/* { this.props.title } */}
        {/* { this.props.Title } */}
        {/* { <this.props.title /> } */}
        {/* {JSON.stringify({title: 100})} */}
      </div>
    );
  }
}

// Child.defaultProps = {
//   url: 'http://qfedu.com'
// }

export default Child;