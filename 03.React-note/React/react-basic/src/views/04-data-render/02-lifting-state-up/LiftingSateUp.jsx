import React, { Component } from 'react'
import Child from './Child'
import Child2 from './Child2'

class LiftingSateUp extends Component {
  state = {
    data: ''
  }

  handleReceiveData = data => {
    this.setState({
      data
    })
  }

  render() {
    return (
      <div>
        <h1>parent</h1>
        <Child 
          title="hello" 
          onReceiveData={this.handleReceiveData}
        ></Child>
        <Child2 data={this.state.data}></Child2>
      </div>
    );
  }
}

export default LiftingSateUp;