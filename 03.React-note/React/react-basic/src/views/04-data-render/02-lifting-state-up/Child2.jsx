import React, { Component } from 'react';

class Child2 extends Component {
  render() {
    return (
      <div>
        { this.props.data }
      </div>
    );
  }
}

export default Child2;