import React, { Component } from 'react';

class Child extends Component {
  handleClick = () => {
    this.props.onReceiveData('hello')
  }

  render() {
    return (
      <>
        <div>
          { this.props.title }
        </div>
        <div>
          <button onClick={this.handleClick}>send</button>
        </div>
      </>
    )
  }
}

export default Child;