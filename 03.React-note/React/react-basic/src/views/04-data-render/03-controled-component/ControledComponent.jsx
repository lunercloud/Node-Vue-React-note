import React, { Component } from 'react';

class ControledComponent extends Component {
  state = {
    inputValue: 'abc'
  }

  handleChange = (e) => {
    this.setState({
      inputValue: e.target.value
    })
  }

  handleInput = (e) => {
    this.setState({
      inputValue: e.target.value
    })
  }

  render() {
    return (
      <div>
        {/* <input
          type="text"
          value={this.state.inputValue}
          onChange={this.handleChange}
        /> */}
        <input
          type="text"
          defaultValue={this.state.inputValue}
          onInput={this.handleInput}
        />
        {this.state.inputValue}
      </div>
    );
  }
}

export default ControledComponent;