import React, { Component } from 'react';

class RenderData extends Component {
  state = {
    show: false,
    list: [
      'a',
      'b',
      'c'
    ],
    h1: '<h1>header</h1>'
  }

  handleClick = () => {
    this.setState((state) => ({
      show: !state.show
    }))
  }

  render() {
    return (
      <div>
        {
          this.state.show && (
            <span>hello</span>
          )
        }
        <button onClick={this.handleClick}>show</button>

        <ul>
          {
            this.state.list.map(li => (<li key={li}>{li}</li>))
          }
        </ul>

        <div dangerouslySetInnerHTML={{__html: this.state.h1}}></div>

        <label htmlFor=""></label>
      </div>
    );
  }
}

export default RenderData;