import React, { Component } from 'react';

class Events extends Component {
  // constructor(props) {
  //   super()
  //   this.handleClick = this.handleClick.bind(this)
  // }

  handleClick = (args) => {
    return (e) => {
      console.log(e.target.nodeName)
      console.log(args)
    }
  }

  // handleClick(arg) {
  //   return (e) => {
  //     console.log(e.target.nodeName)
  //     console.log(arg)
  //   }
  // }

  // handleClick = (args) => {
  //   console.log(args)
  // }

  render() {
    return (
      <div>
        hello
        {/* <button onClick={(e) => this.handleClick('hello')}>click</button> */}
        <button onClick={this.handleClick('hello')}>click</button>
      </div>
    );
  }
}

export default Events;