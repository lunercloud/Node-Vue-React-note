import React, { Component } from 'react'
import { 
  Route,
  Redirect,
  Switch
} from 'react-router-dom'

import Home from './home/Index'
import Detail from './detail/Detail'
import Page404 from './Page404'

import List from './list/List'

import 'animate.css'

class Router extends Component {
  render() {
    return (
      <>
        <Switch>
          <Route
            path="/home"
            component={Home}
          ></Route>
          <Route
            path="/detail/:id"
            component={Detail}
          ></Route>
          <Redirect
            from="/"
            to="/home"
            exact
          ></Redirect>
          <Route
            path="*"
            component={Page404}
          ></Route>
        </Switch>
        {/* <List></List> */}
      </>
    )
  }
}

export default Router;