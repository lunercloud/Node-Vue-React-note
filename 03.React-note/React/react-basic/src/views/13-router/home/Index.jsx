import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'

import '../style.css'

import Home from './Home'
import Menu from './Menu'
import About from './About'
import Search from './Search'
import Cart from './Cart'

class MyLink extends Component {
  handleClick = () => {
    const { navigate, href } = this.props
    console.log(this.props)
    navigate(href)
  }
  
  render() {
    const { children, className} = this.props
    return (
      <li 
        className={className}
        onClick={this.handleClick}
      >{children}</li>
    )
  }
}

class RouterDemo extends Component {
  render() {
    const { path: pathname } = this.props.match
    
    return (
      <div className="wrapper">
        <main>
          {/* <Switch> */}
            <Route
              path={`${pathname}/home`}
            >
              <Home { ...this.props }></Home>
            </Route>
            <Route
              path={`${pathname}/menu`}
              render={ (props) => <Menu { ...props }></Menu> }
            ></Route>
            <Route
              path={`${pathname}/search`}
              component={(props) => <Search { ...props }></Search>}
            >
            </Route>
            {/* <Route
              path={`${pathname}/about`}
              children={(props) => <About {...props}></About>}
            ></Route> */}
            {/* <Route
              path={`${pathname}/cart`}
              children={(props) => <Cart {...props}></Cart>}
            ></Route> */}
            {/* <Cart {...this.props}></Cart> */}
            <Redirect from="/home" to="/home/home"></Redirect>
          {/* </Switch> */}
        </main>
        <ul>
          <NavLink to={`${pathname}/home`} component={MyLink}>首页</NavLink>
          <NavLink to={`${pathname}/menu`} component={MyLink}>分类</NavLink>
          {/* <li><NavLink activeClassName="active" to={`${pathname}/home`}>首页</NavLink></li>
          <li><NavLink activeClassName="active" to={`${pathname}/menu`}>分类</NavLink></li>
          <li><NavLink activeClassName="active" to={`${pathname}/search`}>搜索</NavLink></li>
          <li><NavLink activeClassName="active" to={`${pathname}/about`}>关于</NavLink></li>
          <li><NavLink activeClassName="active" to={`${pathname}/cart`}>购物车</NavLink></li> */}
        </ul>
      </div>
    )
  }
}

export default RouterDemo;