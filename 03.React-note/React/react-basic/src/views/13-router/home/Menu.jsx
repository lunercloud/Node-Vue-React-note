import React, { Component } from 'react'
import { CSSTransition } from 'react-transition-group'

import 'animate.css'

class Menu extends Component {
  state = {
    show: true
  }

  render() {
    // console.log(this.props)
    return (
      <div>
        <CSSTransition
          in={this.state.show}
          timeout={5000}
          classNames={{
            enter: 'animate__animated',
            enterActive: 'animate__slideInLeft',
            exit: 'animate__animated',
            exitActive: 'animate__slideOutLeft',
          }}
          mountOnEnter={true}
          unmountOnExit={true}
        >
          <div>menu</div>
        </CSSTransition>
        <div>
          <button onClick={() => this.setState(state => ({show: !state.show}))}>click</button>
        </div>
      </div>
    );
  }
}

export default Menu;