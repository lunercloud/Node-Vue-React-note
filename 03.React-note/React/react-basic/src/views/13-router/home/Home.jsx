import React, { Component } from 'react';

class Home extends Component {
  handleClick = () => {
    let { push, watch } = this.props.history
    push('/detail/200?name=gp22', {
      x: 100
    })
    console.log(watch)
    
  }

  render() {
    return (
      <div>
        home
        <div>
          <button
            onClick={this.handleClick}
          >goto detail</button>
        </div>
      </div>
    );
  }
}

export default Home;