// import React, { Component } from 'react';

// class Detail extends Component {
//   render() {
//     console.log(this.props)
//     let { search } = this.props.location
//     let query = new URLSearchParams(search)
//     console.log(query.get('name'))
    
//     return (
//       <div>
//         detail 
//         {this.props.location.state.x}
//         {this.props.match.params.id}
//       </div>
//     );
//   }
// }

// export default Detail;

import React from 'react'
import {
  useHistory,
  useLocation,
  useParams,
  useRouteMatch,
  Prompt
} from 'react-router-dom'

export default function Detail(props) {
  let history = useHistory()
  let location = useLocation()
  let params = useParams()
  let math = useRouteMatch()

  // history.block(() => {
  //   return window.confirm('ok?')
  // })

  // 组件内路由守卫 *****
  history.listen(() => {
    console.log('listen')
  })

  const handleClick = (args) => {
    return () => {
      history.push('/detail/' + args )
    }
  }

  return (
    <>
      <div>
        {location.search}
        {params.id}
        <div>
          <button onClick={handleClick(300)}>btn1</button>
          <button onClick={handleClick(500)}>btn2</button>
        </div>
      </div>
    </>
  )
}
