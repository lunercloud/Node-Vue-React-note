import React from 'react'
import {
  useHistory
} from 'react-router-dom'

export default function List() {
  let { push } = useHistory()
  
  const handleClick = () => {
    push('/home/cart')
  }

  return (
    <div>
      <button onClick={handleClick}>cart</button>
    </div>
  )
}
