import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Child from './Child'

class LifeTimes extends Component {
  constructor () {
    super()
    this.state = {
      title: 'hello'
    }
    // this.handleClick = this.handleClick.bind(this)
  }

  UNSAFE_componentWillMount() {
    console.log('willmount')
  }

  componentDidMount() {
    console.log('didmount')
    this.setState({
      title: 'hello world'
    })

    setTimeout(() => {
      ReactDOM.unmountComponentAtNode(document.querySelector('#root'))
    }, 5000)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.title === this.state.title) {
      return false
    }

    return true
  }

  UNSAFE_componentWillUpdate() {
    console.log('componentWillUpdate')
  }

  componentDidUpdate() {
    console.log('componentDidUpdate')
  }

  render() {
    console.log('render')
    
    return (
      <div>
        {this.state.title}
        <Child title={this.state.title}></Child>
      </div>
    )
  }

  componentWillUnmount() {
    console.log('componentWillUnmount')
  }
}

export default LifeTimes;