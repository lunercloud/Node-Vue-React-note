import React, { Component } from 'react'
import memoize from 'memoize-one'

class Child extends Component {
  state = {
    // name: ''
    // prevColor: '',
    color: this.props.color,
    keywords: 'abc'
  }

  filteredList = memoize((keywords, list) => {
    return list.filter(item => item === keywords)
  })

  // computed: {
  //   filteredList: (keywords, list) => {
  //     return list.filter(item => item === keywords)
  //   }
  // }

  // static getDerivedStateFromProps(nextProps, nextState) {
  //   // console.log(nextProps, nextState)
  //   if (nextProps.color === nextState.prevColor) {
  //     // 如果在组建的内部更新的state, 不需要在这个钩子里重复的赋值
  //     return {
  //       prevColor: nextState.color
  //     }
  //   } else {
  //     return {
  //       color: nextProps.color,
  //       prevColor: nextProps.color
  //     }
  //   }
  // }

  componentDidMount() {
    // setTimeout(() => {
    //   this.setState({
    //     title: 'world'
    //   })
    // }, 5000)
  }

  // UNSAFE_componentWillReceiveProps(nextProps) {
  //   console.log(nextProps.title)
  // }

  handleClick = () => {
    // this.setState({
    //   color: 'green'
    // })
    this.setState({
      keywords: 'abc'
    })
  }

  render() {
    // console.log('child component rendered.')
    
    return (
      <div style={{color: this.state.color}}>
        {/* {this.state.name}
        {this.state.title} */}
        {/* 大字报
        <div>
          <button onClick={this.handleClick}>change color</button>
        </div> */}

        {
          this.filteredList(this.state.keywords, this.props.list)
            .map((value, index) => {
              return (
                <div key={value + index}>{value}</div>
              )
            })
        }
        <button onClick={this.handleClick}>filter</button>
      </div>
    )
  }
}

export default Child;