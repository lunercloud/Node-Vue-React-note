import React, { Component } from 'react'
import withTitle from './withTitle'

@withTitle
class Hoc extends Component {
  render() {
    return (
      <div>
        hello
      </div>
    )
  }

  componentDidMount() {
    console.log(this.props)
  }
}

export default Hoc