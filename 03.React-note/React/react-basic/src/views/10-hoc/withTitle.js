import React, { Component } from 'react'

function withTitle(Comp) {
  return class extends Component {
    render() {
      return (
        <Comp
          title="hello"
        ></Comp>
      )
    }
  }
}

export default withTitle