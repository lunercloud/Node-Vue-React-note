import React, { useState, useCallback, useMemo } from 'react'
import Child from './Child'

export default function CallbackDemo() {
  let [title, setTitle] = useState({
    text: 'hello'
  })

  let [color, setColor] = useState('red')
  
  let child = useMemo(() => {
    return (
      <Child title={title}></Child>
    )
  }, [title])

  let title2 = useMemo(() => {
    console.log(100)
    return {
      text: title.text + '!!!'
    }
  }, [title])
  
  const handleClick = useCallback(
    (x) => {
      return () => {
        setTitle({
          text: 'hello'
        })
      }
      // return () => {
      //   setColor(x)
      // }
    },
    []
  )

  return (
    <div>
      <div>{color}</div>
      {/* <Child onRun={() => { console.log(100) }}></Child> */}
      { child }
      <button
        onClick={handleClick('x')}
      >
        click
      </button>
      {title2.text}
    </div>
  )
}