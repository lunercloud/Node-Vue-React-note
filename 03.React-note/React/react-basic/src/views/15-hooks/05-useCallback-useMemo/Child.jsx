import React, { useEffect, memo } from 'react'

function Child(props) {
  console.log('child')

  // useEffect(() => {
  //   console.log(props.title)
  // }, [props.title])

  return (
    <div>
      child
    </div>
  )
}

export default Child

// import React, { useMemo } from 'react'

// const Child1 = () => {
//   console.log(0)
//   return (
//     <div>
//       child1
//     </div>
//   )
// }
// const Child2 = () => {
//   console.log(1)
//   return (
//     <div>
//       child2
//     </div>
//   )
// }

// function Child(props) {
//   // Only re-rendered if `a` changes:
//   const Child11 = useMemo(() => () => <Child1 a={props.title} />, [props.title]);
//   // Only re-rendered if `b` changes:
//   const Child21 = useMemo(() => () => <Child2 b={props.title} />, [props.title]);
//   return (
//     <>
//       {<Child11></Child11>}
//       {<Child21></Child21>}
//     </>
//   )
// }

// export default Child
