import React, { useState, useEffect } from 'react'

export default function UseEffectDemo() {
  let [ title, setTitle ] = useState('hello')
  let [ count, setCount ] = useState(100)

  // useEffect(() => {
  //   // console.log(document.querySelector('#box').innerHTML)
  //   // setTitle需要传入两种参数，一种不依赖于state的新值，另一种是依赖原有state的值，
  //   // 此时需要将SetTitle的参数变成一个回到函数，函数的形参是state
  //   setTitle((title) => title + 1)
  // }, [])

  // useEffect(() => {
  //   console.log(title)
  //   console.log(count)
  // }, [title, count])

  // useEffect(() => {
  //   setInterval(() => {
  //     // setTitle(title => title + ' world')
  //     setTitle(title => title)
  //     setCount(count => count + 1)
  //   }, 1000)
  // }, [])

  useEffect(() => {
    console.log(0)

    return () => {
      console.log('will unmount')
    }
  }, [])

  useEffect(() => {
    console.log(0)

    return () => {
      console.log('will unmount')
    }
  }, [title])

  return (
    <div id="box">
      {title}
    </div>
  )
}
