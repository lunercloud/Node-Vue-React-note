import React, { useState } from 'react'

export default function UseStateDemo() {
  // let [ count, setState ] = useState(0)
  // let [ title, setTitle ] = useState('hello')

  let [ data, setData ] = useState({
    count: 0,
    title: 'hello'
  })

  return (
    <div>
      {data.count} {data.title}
      <button onClick={() => setData({...data, count: 100})}>+</button>
      <button onClick={() => setData({...data, title: 'world'})}>change</button>
    </div>
  )
}
