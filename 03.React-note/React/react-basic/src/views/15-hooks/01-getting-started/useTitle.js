function useTitle (count) {
  const changeTitle = () => {
    document.title = count
  }

  return {
    changeTitle
  }
}

export default useTitle