import { useState } from 'react'

function useCount () {
  let [count, setCount] = useState(0)

  const minus = () => {
    setCount(count - 1)
  }

  const add = () => {
    setCount(count + 1)
  }

  return {
    count,
    minus,
    add
  }
}

export default useCount