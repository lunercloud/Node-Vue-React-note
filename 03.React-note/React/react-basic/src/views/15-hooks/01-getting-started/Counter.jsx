import React, { Component } from 'react';

class Counter extends Component {
  state = {
    count: 0
  }

  minus = () => {
    this.setState({
      count: this.state.count - 1
    })
  }

  add = () => {
    this.setState({
      count: this.state.count + 1
    })
  }

  changeTitle = () => {
    document.title = this.state.count
  }

  render() {
    return (
      <>
        <div>
          <button onClick={this.minus}>-</button>
          {this.state.count}
          <button onClick={this.add}>+</button>
        </div>
        <div>
          <button onClick={this.changeTitle}>change title</button>
        </div>
      </>
    );
  }
}

export default Counter;