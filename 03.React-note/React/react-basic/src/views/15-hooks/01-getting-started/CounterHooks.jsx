import React from 'react'
import useCount from './useCount'
import useTitle from './useTitle'

function CounterHooks () {
  let { minus, add, count } = useCount()
  let { changeTitle } = useTitle(count)

  return (
    <>
      <div>
        <button onClick={minus}>-</button>
        {count}
        <button onClick={add}>+</button>
      </div>
      <div>
        <button onClick={changeTitle}>change title</button>
      </div>
    </>
  )
}

export default CounterHooks