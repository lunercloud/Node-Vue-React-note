import React, { createContext } from 'react'

const myContext = createContext({
  color: 'red'
})

export default myContext