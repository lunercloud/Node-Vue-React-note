import React, { useContext } from 'react'
import myContext from './myContext'
import yourContext from './yourContext'

export default function ContextDemo() {
  const { color } = useContext(myContext)
  const title = useContext(yourContext)
  return (
    <div>
      {/* <myContext.Consumer>
        {
          (value) => {
            return value.color
          }
        }
      </myContext.Consumer> */}
      {
        <div>
          {color}
          <div>
            {title}
          </div>
        </div>
      }
    </div>
  )
}
