import React, { useRef, useEffect, forwardRef, useImperativeHandle } from 'react'

const MyInput = forwardRef(
  function (props, ref) {
    const inputRef = useRef()

    useImperativeHandle(ref, () => inputRef.current)
  
    return (
      <input type="text" ref={inputRef}/>
    )
  }
)

export default function ImperativeHandle() {
  const inputRef = useRef(null)

  useEffect(() => {
    // console.log(inputRef)
    inputRef.current.focus()
  }, [])

  return (
    <div>
      <MyInput value={'hello'} ref={inputRef}></MyInput>
    </div>
  )
}
