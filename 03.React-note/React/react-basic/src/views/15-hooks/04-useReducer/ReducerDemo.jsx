import React, { useReducer } from 'react'

export default function ReducerDemo() {
  const defaultState = {
    count: 0
  }

  const reducer = (state, action) => {
    switch(action.type) {
      case 'add':
        return {
          count: state.count + 1
        }

      case 'minus':
        return {
          count: state.count + 1
        }

      default: 
        return state
    }
  }

  const [state, dispatch] = useReducer(reducer, defaultState)

  return (
    <div>
      {state.count}
      <button onClick={() => dispatch({ type: 'add' })}>+</button>
    </div>
  )
}
