import React, { useState, useEffect, useRef } from 'react'

// function App() {
//   const [count, setCount] = useState(0);

//   useEffect(() => {
//     setTimeout(() => {
//      console.log("count: " + count);
//     }, 3000);
//   }, [count]);

//   return (
//     <div>
//       <p>You clicked {count} times</p>
//       <button onClick={() => setCount(count + 1)}>增加 count</button>
//       <button onClick={() => setCount(count - 1)}>减少 count</button>
//     </div>
//   );
// }

// export default App

// class App extends React.Component {
//   state = {
//     count: 0
//   }

//   componentDidUpdate() {
//     setTimeout(() => {
//       console.log(this.state.count)
//     }, 3000)
//   }

//   render() {
//     return (
//       <div>
//         <p>You clicked {this.state.count} times</p>
//         <button onClick={() => this.setState({
//           count: this.state.count + 1
//         })}>增加 this.count</button>
//         <button onClick={() => this.setState({
//           count: this.state.count - 1
//         })}>减少 count</button>
//       </div>
//     )
//   }
// }

// export default App


function App() {
  const count = useRef(0)

  const setCount = () => {
    count.current += 1
    setTimeout(() => {
      console.log(count.current)
    }, 3000);
  }

  // useEffect(() => {
  //   setTimeout(() => {
  //     console.log(count.current)
  //   }, 3000);
  // }, [count])

  return (
    <div>
      <p>You clicked {count.current} times</p>
      <button onClick={() => setCount(count + 1)}>增加 count</button>
    </div>
  );
}

export default App