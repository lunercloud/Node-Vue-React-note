import React, { createRef, useState, useRef, useCallback } from 'react'

export default function RefDemo() {
  // const ref = createRef()
  const [ count, setCount ] = useState(100)

  const ref = useRef()

  const message = useRef({
    title: 'content'
  })

  const handleClick = useCallback(
    () => {
      console.log(ref.current)
      setCount(200)
      message.current.title = 'abc'
    },
    [ref],
  )

  return (
    <div ref={ref}>
      hello
      <div>
        {count}
      </div>
      <div>
        {message.current.title}
      </div>
      <div>
        <button onClick={handleClick}>click</button>
      </div>
    </div>
  )
}
