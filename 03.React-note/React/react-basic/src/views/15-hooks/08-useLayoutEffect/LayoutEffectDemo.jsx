import React, { useEffect, useLayoutEffect, useRef } from 'react'

function moveTo(dom, delay, options) {
  dom.style['transform'] = `translate(${options.value}px, 0)`
  dom.style['transition'] = `all ${delay}ms`
}

export default function LayoutEffectDemo() {
  const divRef = useRef(null)

  useEffect(() => {
    moveTo(divRef.current, 300, {
      value: 200
    })
  }, [])

  // useLayoutEffect(() => {
  //   moveTo(divRef.current, 300, {
  //     value: 200
  //   })
  // }, [])

  return (
    <div style={{width: '100%', height: '500px', background: 'yellowgreen'}}>
      <div
        ref={divRef}
        style={{width: '100px', height: '100px', background: 'yellow'}}
      ></div>
    </div>
  )
}
