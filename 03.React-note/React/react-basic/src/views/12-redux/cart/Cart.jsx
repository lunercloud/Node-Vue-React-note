import React, { Component } from 'react'
import { Products } from './products'
import { Carts } from './carts'

class Cart extends Component {
  render() {
    return (
      <div>
        <h1>产品列表</h1>
        <hr/>
        <Products></Products>
        <h1>购物车</h1>
        <hr/>
        <Carts></Carts>
      </div>
    );
  }
}

export default Cart;