import Products from './Products'
import reducer from './reducer'

export {
  Products,
  reducer
}