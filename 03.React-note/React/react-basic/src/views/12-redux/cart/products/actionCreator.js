import axios from 'axios'
import { DECREMENTINVENTORY } from './actionTypes'

import {
  LOADDATA
} from './actionTypes'

export const loadDataSync = (products) => {
  return {
    type: LOADDATA,
    products
  }
}

export const loadDataAsync = () => {
  return async (dispatch) => {
    let result = await axios({
      url: '/products.json'
    })
    dispatch(loadDataSync(result.data))
  }
}

export const decrementInventory = product => {
  return {
    type: DECREMENTINVENTORY,
    product
  }
}