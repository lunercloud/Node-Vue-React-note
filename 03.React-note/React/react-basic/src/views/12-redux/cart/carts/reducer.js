import { ADDPRODUCT, TOTALPRICE } from './actionTypes'

const defaultState = {
  products: [],
  total: 0
}

const reducer = (state = defaultState, action) => {
  switch(action.type) {
    case ADDPRODUCT:
      // 获取传递过来的product, 放入购物车
      const product = action.product

      // 为了不改变原值，要克隆一份数组
      const products = [
        ...state.products
      ]

      // 在新数组中查找符合条件的产品
      const result = products.find(p => p.id === product.id)
      
      // 如果找到了，就把该产品的数量加1
      if (result) {
        result.quantity++
        return {
          ...state,
          products
        }
      
      // 如果没有找到，就添加一个新产品，数量为1
      } else {
        products.push({
          ...product,
          quantity: 1
        })
        return {
          ...state,
          products
        }
      }
      
    case TOTALPRICE: 
      const total = state.products.reduce((sum, v) => {
        sum += v.price * v.quantity
        return sum
      }, 0)

      return {
        ...state,
        total
      }

    default: 
     return state
  }
}

export default reducer