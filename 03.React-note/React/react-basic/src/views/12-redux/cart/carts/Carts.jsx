import React, { Component } from 'react'
import { connect } from 'react-redux'
import CartsUi from './CartsUi'

@connect(
  state => ({
    products: state.cart.products,
    totalPrice: state.cart.total
  })
)
class Carts extends Component {
  render() {
    return (
      <CartsUi 
        products={this.props.products}
        totalPrice={this.props.totalPrice}
      ></CartsUi>
    )
  }
}

export default Carts;