import React from 'react'

export default function CartsUi(props) {
  return (
    <>
      <ul>
        {
          props.products.map(product => {
            return (
              <li key={product.id}>{product.name}: {product.price} x {product.quantity} = {product.price * product.quantity}</li>
            )
          })
        }
      </ul>
      <div>总价：{props.totalPrice}</div>
    </>
  )
}
