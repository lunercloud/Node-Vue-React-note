import { ADDPRODUCT, TOTALPRICE } from './actionTypes'

export const addProduct = (product) => {
  return {
    type: ADDPRODUCT,
    product
  }
}

export const totalPrice = () => {
  return {
    type: TOTALPRICE
  }
}