function createM1(extraArgument) {
  return ({ dispatch, getState }) => next => action => {    
    if (action.payload === 'm1') {
      next(action)
    } else {
      next({
        type: 'test',
        payload: 'hello'
      })
    }
  }
}

const m1 = createM1();
m1.withExtraArgument = createM1;

export default m1