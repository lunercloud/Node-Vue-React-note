export const mulCreator= (num, payload) => {
  return {
    type: 'multiple',
    num,
    payload
  }
}

export const subCreator = (num) => {
  return {
    type: 'substract',
    num
  }
}
