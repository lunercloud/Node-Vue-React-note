import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import m1 from '../middlewares/m1'
import m2 from '../middlewares/m2'

import reducerRoot from './reducerIndex'

const store = createStore(reducerRoot, applyMiddleware(
  m1,
  m2,
  thunk
))

export default store