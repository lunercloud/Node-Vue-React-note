const defalutState = {
  n: 1
}

const reducer = (state = defalutState, action) => {
  switch(action.type) {
    case 'multiple':
      return {
        n: action.num * state.n
      }
    case 'substract':
      return {
        n: state.n / action.num
      }
    case 'test':
      return {
        n: 10000
      }

    default:
      return state
  }
}

export default reducer