import { combineReducers } from 'redux'
import am from './reducer'
import ms from './reducer2'

const reducerRoot = combineReducers({
  am,
  ms
})

export default reducerRoot