import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import store from './store'

import * as Actions1 from './store/actionCreator1'
import * as Actions2 from './store/actionCreator2'

class Counter extends Component {
  render() {

    const actions = {
      ...bindActionCreators(Actions1, store.dispatch),
      ...bindActionCreators(Actions2, store.dispatch)
    }

    return (
      <div>
        {/* {store.getState().am.count} */}
        {store.getState().ms.n}
        <button onClick={() => actions.mulCreator(20, 'm1')}>x</button>
        <button onClick={() => actions.subCreator(2)}>÷</button>
      </div>
    );
  }
}

export default Counter;