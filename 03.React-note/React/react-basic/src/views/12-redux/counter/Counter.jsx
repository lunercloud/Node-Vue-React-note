import React, { Component } from 'react'
import store from './store'

class Counter extends Component {
  handleMinus = () => {
    store.dispatch({type: 'minus'})
  }

  handleAdd = () => {
    store.dispatch({type: 'add'})
  }

  render() {
    return (
      <div>
        <button onClick={this.handleMinus}>-</button>
        {store.getState().count}
        <button onClick={this.handleAdd}>+</button>
      </div>
    );
  }
}

export default Counter;