export const addCreator= (num) => {
  return {
    type: 'add',
    num
  }
}

export const minusCreator = () => {
  return {
    type: 'minus'
  }
}

export const asyncAddCreator = () => {
  return (dispatch) => {
    setTimeout(() => {
      dispatch(addCreator())
    }, 3000)
  }
}