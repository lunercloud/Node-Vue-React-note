const defalutState = {
  count: 0
}

const reducer = function(state = defalutState, action) {
  switch(action.type) {
    case 'add':
      return {
        count: state.count + action.num
      }
    case 'minus':
      return {
        count: state.count - 1
      }
    default: 
      return state
  }
}

export default reducer