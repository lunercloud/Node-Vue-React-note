import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Child from './Child'
import * as countActions from './store/actionCreators'


// react-redox的功能：
// 1、通过它访问和返回新的state
// 2、可以响应state发生的变化

const mapStateToProps = state => ({
  count: state.count
})

// const mapDispatchToProps = dispatch => ({
//   add() {
//     dispatch(counter('add'))
//   },
//   minus() {
//     dispatch(counter('minus'))
//   }
// })

const mapDispatchToProps = dispatch => {
  let result = bindActionCreators(countActions, dispatch)
  // console.log(result)
  return result
}

// connect 是个函数, connect() 执行结果是个高阶组件
@connect(
  mapStateToProps,
  mapDispatchToProps
)
class Counter extends Component {
  render() {
    return (
      <div>
        <button onClick={this.props.minusCreator}>-</button>
        {this.props.count}
        <button onClick={this.props.addCreator}>+</button>
        <button onClick={this.props.asyncAddCreator}>async + </button>
        <Child></Child>
      </div>
    );
  }
}

export default Counter;