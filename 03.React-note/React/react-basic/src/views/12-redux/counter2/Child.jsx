import React, { Component } from 'react'
import { connect } from 'react-redux'

const mapStateToProps = state => ({
  count: state.count
})

@connect(mapStateToProps)
class Child extends Component {
  render() {
    return (
      <div>
        {this.props.count}
      </div>
    );
  }
}

export default Child;