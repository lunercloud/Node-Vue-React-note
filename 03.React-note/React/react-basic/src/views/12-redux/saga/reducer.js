const defaultState = {
  n: 100
}

const reducer = (state = defaultState, action) => {
  if (action.type === 'add') {
    return {
      n: action.data
    }
  }
  return state
}

export default reducer