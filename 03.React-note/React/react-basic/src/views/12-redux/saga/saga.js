import { call, put, takeEvery } from 'redux-saga/effects'
import axios from 'axios'

function* loadData() {
  const result = yield axios({
    url: '/products.json'
  })
  yield put({
    type: 'add',
    data: result.data
  })
}

function* saga() {
  yield takeEvery('loadData', loadData)
}

export default saga