import React, { Component } from 'react'
import { connect } from 'react-redux'

@connect(
  state => {
    return {
      n: state.n
    }
  },
  dispatch => {
    return {
      add() {
        dispatch({
          type: 'loadData'
        })
      }
    }
  }
)
class Saga extends Component {
  render() {
    return (
      <div>
        <div>{JSON.stringify(this.props.n)}</div>
        <div><button onClick={this.props.add}>add</button></div>
      </div>
    );
  }
}

export default Saga;