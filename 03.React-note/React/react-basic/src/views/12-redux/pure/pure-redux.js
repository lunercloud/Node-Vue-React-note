import { createStore } from 'redux'

import reducer from './reducer.js'

const store = createStore(reducer)

const unSubscribe = store.subscribe(() => {
  console.log(store.getState())
})

unSubscribe()

store.dispatch({
  type: 'add'
})