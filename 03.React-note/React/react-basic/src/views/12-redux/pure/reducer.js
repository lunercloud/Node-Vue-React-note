const defaultState = {
  count: 0
}

function reducer(state = defaultState, action) {
  switch(action.type) {
    case 'add':
      return {
        count: state.count + 1
      }
    case 'minus':
      return {
        count: state.count - 1
      }
    default: 
      return state
  }
}

export default reducer