import { ADDPRODUCT, TOTALPRICE } from '../action-types/carts'

export const addProduct = (product) => {
  return {
    type: ADDPRODUCT,
    product
  }
}

export const totalPrice = () => {
  return {
    type: TOTALPRICE
  }
}