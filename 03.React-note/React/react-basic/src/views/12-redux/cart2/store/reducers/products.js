import { LOADDATA, DECREMENTINVENTORY } from '../action-types/products'

const defaultState = {
  all: []
}

// 纯纯纯函数
const reducer = (state = defaultState, action) => {
  switch(action.type) {
    case LOADDATA:
      return {
        ...state,
        all: action.products
      }
      
    case DECREMENTINVENTORY:
      const product = action.product
      const products = [
        ...state.all
      ]
      
      // 找到该产品，库存减一
      const result = products.find(p => p.id === product.id)
      result.inventory--

      return {
        all: products
      }

    default:
      return state
  }
}

export default reducer