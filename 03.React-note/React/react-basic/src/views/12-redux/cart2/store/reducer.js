import { combineReducers } from 'redux'
import product from './reducers/products'
import cart from './reducers/carts'

const reducer = combineReducers({
  product,
  cart
})

export default reducer