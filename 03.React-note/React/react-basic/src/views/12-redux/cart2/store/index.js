import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import m1 from '../middlewares/m1'
import m2 from '../middlewares/m2'

import reducer from './reducer'

const store = createStore(
  reducer,
  applyMiddleware(
    m1,
    m2,
    thunk.withExtraArgument('hello')
  )
)

export default store
