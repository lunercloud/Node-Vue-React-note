import axios from 'axios'
import { DECREMENTINVENTORY } from '../action-types/products'

import {
  LOADDATA
} from '../action-types/products'

export const loadDataSync = (products) => {
  return {
    type: LOADDATA,
    products
  }
}

export const loadDataAsync = () => {
  return async (dispatch, getState, extraArgument) => {
    // console.log(extraArgument)
    let result = await axios({
      url: '/products.json'
    })
    dispatch(loadDataSync(result.data))
  }
}

export const decrementInventory = product => {
  return {
    type: DECREMENTINVENTORY,
    product
  }
}