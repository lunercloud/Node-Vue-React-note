function createM2(extraArgument) {
  return ({ dispatch, getState }) => next => action => {    
    next(action)
  }
}

const m2 = createM2()
m2.withExtraArgument = m2

export default m2