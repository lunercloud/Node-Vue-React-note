function createM1(extraArgument) {
  return ({ dispatch, getState }) => next => action => {    
    next(action)
  }
}

const m1 = createM1();
m1.withExtraArgument = m1;

export default m1