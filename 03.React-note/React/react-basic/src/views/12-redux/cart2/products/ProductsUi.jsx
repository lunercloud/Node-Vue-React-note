// UI组件 木偶组件(dumb component) 展示组件(Presentational Component)
import React from 'react'

export default function ProductsUi(props) {
  // console.log(props.products)
  return (
    <ul>
      {
        props.products.map((product) => {
          return (
            <li key={product.id}>
              {product.name} - {product.price}
              <button 
                onClick={props.addProductToCart(product)}
                disabled={product.inventory <= 0}
              >放入购物车</button>
            </li>
          )
        })
      }
    </ul>
  )
}
