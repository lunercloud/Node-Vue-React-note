import React from 'react'
import ReactDOM from 'react-dom'
// import store from './views/12-redux/counter2/store/index'
// import store from './views/12-redux/counter3/store/index'
// import store from './views/12-redux/cart/store'
// import store from './views/12-redux/cart2/store'
// import store from './views/12-redux/saga/store'
// import { Provider } from 'react-redux'

// import { Provider } from './views/09-context/context/listContext'
// import { CounterProvider } from './views/09-context/sample2/counterContext'

// import { BrowserRouter } from 'react-router-dom'

// import App from './views/01-getting-started/App'
// import App from './views/02-jsx/Jsx'
// import App from './views/03-styles/Styles'
// import App from './views/04-data-render/DataRender'
// import App from './views/04-data-render/02-lifting-state-up/LiftingSateUp'
// import App from './views/04-data-render/03-controled-component/ControledComponent'
// import App from './views/04-data-render/04-render-data/RenderData'
// import App from './views/05-events/Events'
// import App from './views/06-form/Form'
// import App from './views/07-todolist/TodoList'
// import App from './views/08-lifetimes/LifeTimes'
// import App from './views/09-context/Index'
// import App from './views/09-context/sample2/Counter'
// import App from './views/10-hoc/Hoc'
// import App from './views/11-portal/Page'
// import App from './views/12-redux/counter/Counter'
// import App from './views/12-redux/counter2/Counter'
// import App from './views/12-redux/cart/Cart'
// import App from './views/12-redux/cart2/Cart'
// import App from './views/12-redux/counter3/Counter'
// import App from './views/12-redux/saga/SagaComponent'
// import App from './views/15-hooks/Counter'
// import App from './views/15-hooks/CounterHooks'
// import App from './views/15-hooks/02-useState/UseStateDemo'
// import App from './views/15-hooks/02-useState/UseEffectDemo'
// import App from './views/15-hooks/03-useContext/ContextDemo'
// import App from './views/15-hooks/04-useReducer/ReducerDemo'
// import App from './views/15-hooks/05-useCallback/CallbackDemo'
// import App from './views/15-hooks/06-useRef/RefDemo'
// import App from './views/15-hooks/06-useRef/CaptureValue'
// import App from './views/15-hooks/07-useImperativeHandle/ImperativeHandle'
// import App from './views/15-hooks/08-useLayoutEffect/LayoutEffectDemo'
// import App from './views/15-hooks/09-customHooks/Auth'
// import App from './views/15-hooks/09-customHooks/Recursive'
// import App from './views/15-hooks/09-customHooks/RouterConfig'
import App from './views/16-render-props/RenderProps'

function render() {
  ReactDOM.render(
    // <Provider value={['a', 'b', 'c']}>
    // <CounterProvider>
    // <Provider store={store}>
      // <BrowserRouter><App></App></BrowserRouter>
      <App></App>,
    // </Provider>,
    // </CounterProvider>,
    // </Provider>,
    document.querySelector('#root')
  )
}

render()

// store.subscribe(render)