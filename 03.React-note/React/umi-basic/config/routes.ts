export default [
  // { 
  //   exact: true, 
  //   path: '/index2', 
  //   component: './index2', 
  //   wrappers: [
  //     '@/pages/users/auth',
  //   ]
  // },
  // umi 路由使用注意事项：
  // 1、path 子路由必须不能加 /
  // 2、redirect 要单独定义
  {
    exact: true,
    path: '/',
    redirect: '/index'
  },
  {
    path: '/index', 
    component: './index',
    routes: [
      {
        exact: true,
        path: '/index',
        redirect: '/index/carts'
      },
      {
        path: 'users',
        component: './users/index'
      },
      {
        path: 'carts',
        component: './carts/index'
      }
    ]
  }
]