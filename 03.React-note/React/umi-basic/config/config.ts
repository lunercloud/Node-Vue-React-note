import routes from './routes'

export default {
  base: '/',
  publicPath: '/static/',
  history: {
    type: 'browser',
  },
  // routes: [
  //   { path: '/', component: '@/pages/index2' },
  // ]
  nodeModulesTransform: {
    type: 'none',
  },
  layout: {},
  fastRefresh: {},
  // routes
}