import { IRouteComponentProps } from 'umi'

export default function Layout({ children, location, route, history, match }: IRouteComponentProps) {
  if (location.pathname === '/users') {
    return (
      <div>
        <h2>users</h2>
        {children}
      </div>
    )
  }
  return (
    <div>
      <h1>home</h1>
      {children}
    </div>
  )
}
