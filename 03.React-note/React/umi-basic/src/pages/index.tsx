import React, { useEffect, isValidElement } from 'react'
import './index.scss';

export default function IndexPage(props) {
  useEffect(() => {
    console.log(isValidElement(IndexPage))
  }, [])

  return (
    <div>
      <h1 className={'title'}>Page index</h1>
      <div>
        {/* {
          React.Children.map(props.children, child => {
            return React.cloneElement(child, { foo: 'bar' });
          })
        } */}
        {props.children}
      </div>
    </div>
  );
}
