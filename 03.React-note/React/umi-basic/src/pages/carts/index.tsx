import React, { ReactElement } from 'react'

interface Props {
  title: string
}

function Index(props: Props): ReactElement {
  console.log(props)
  return (
    <div>
      index
    </div>
  )
}

Index.wrappers = ['@/hoc/auth']
Index.title = 'abc'
Index.surname = 'hello'

export default Index
