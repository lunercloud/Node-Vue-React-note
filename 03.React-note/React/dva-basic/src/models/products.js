import { query } from '../services/example'

export default {
  namespace: 'products',
  state: [],
  reducers: {
    'delete'(state, {payload: id}) {
      return state.filter(item => item.id !== id);
    },
    'setProducts'(state, { products }) {
      return products
    },
    'users/fetch'(state, action) {
      console.log(0)
      return state
    }
  },
  effects: {
    *loadData(action, { call, put }) {
      let result = yield call(query, '/data.json')
      yield put({
        type: 'setProducts',
        products: result.data.products
      })
    }
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen(({ pathname }) => {
        console.log(pathname)
        if (pathname === '/users') {
          dispatch({
            type: 'users/fetch',
          });
        }
      });
    },
    keyEvent({dispatch}) {
      // console.log(100)
    }
  }
}