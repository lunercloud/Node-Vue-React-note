import React from 'react';
import { router } from 'dva';
import IndexPage from './routes/IndexPage';

let { Router, Route, Switch } = router

function RouterConfig(context) {
  return (
    <Router history={context.history}>
      <Switch>
        <Route path="/" exact component={IndexPage} />
      </Switch>
    </Router>
  );
}

export default RouterConfig;
