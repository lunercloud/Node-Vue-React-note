import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import { Table, Popconfirm, Button } from 'antd';
import { connect } from 'dva'

const ProductList = ({products, onDelete, loadData}) => {
  const columns = [
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name'
    }, 
    {
      title: '操作',
      key: 'oparation',
      render: (text, record) => {
        return (
          <Popconfirm title="Delete?" onConfirm={() => onDelete(record.id)}>
            <Button>Delete</Button>
          </Popconfirm>
        );
      },
    }
  ];

  useEffect(() => {
    loadData()
  }, [])

  return (
    <Table
      dataSource={products}
      columns={columns}
      rowKey="id"
    />
  );
};

ProductList.propTypes = {
  onDelete: PropTypes.func.isRequired,
  products: PropTypes.array.isRequired,
};

export default connect(
  (state) => {
    return {
      products: state.products
    }
  },
  (dispatch) => {
    return {
      onDelete(id) {
        dispatch({
          type: 'delete',
          payload: id
        })
      },
      loadData() {
        dispatch({
          type: 'products/loadData'
        })
      }
    }
  }
)(ProductList)