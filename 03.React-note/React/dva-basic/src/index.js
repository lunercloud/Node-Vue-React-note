import dva from 'dva';
import './index.css';
import React from 'react';
import { router } from 'dva';
import IndexPage from './routes/IndexPage';

let { Router, Route, Switch } = router

// 1. Initialize
const app = dva();

// 2. Plugins
// app.use({});

// 3. Model
// app.model(require('./models/example').default);
app.model(require('./models/products').default);

// 4. Router
app.router(({history}) => {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={IndexPage} />
      </Switch>
    </Router>
  )
})

// 5. Start
app.start('#root');

// console.log(app._store);
