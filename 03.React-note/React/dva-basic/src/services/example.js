import request from '../utils/request';

export function query(url) {
  return request(url);
}
