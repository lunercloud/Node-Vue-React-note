import React from 'react';
import ProductList from '../components/ProductList'
import { router } from 'dva'
const { Route } = router

function IndexPage() {
  return (
    <ProductList>
      <Route></Route>
    </ProductList>
  )
}

export default IndexPage
